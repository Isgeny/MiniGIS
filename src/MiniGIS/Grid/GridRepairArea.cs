﻿namespace MiniGIS.Grid
{
    using System;

    using MiniGIS.Vector;

    public class GridRepairArea
    {
        private const int MINIMUM_NEAREST_COUNT = 100;

        private const int MINIMUM_RADIUS = 100;

        private const double NEAREST_COUNT_PERCENT = 0.1;

        private const double RADIUS_PERCENT = 0.1;

        private VectorLayer _vectorLayer;

        public GridRepairArea(VectorLayer layer)
        {
            Power = 2;
            NearestCount = CalculateNearestCount(layer);
            SearchRadius = CalculateRadius(layer);
        }

        public int NearestCount { get; set; }

        public PointsSearchMethod PointsSearchMethod { get; set; }

        public int Power { get; set; }

        public double SearchRadius { get; set; }

        public VectorLayer VectorLayer
        {
            get => _vectorLayer;
            set
            {
                _vectorLayer = value;
                NearestCount = CalculateNearestCount(_vectorLayer);
                SearchRadius = CalculateRadius(_vectorLayer);
            }
        }

        private static int CalculateNearestCount(VectorLayer layer)
        {
            if (layer == null)
                return 0;

            var pointsCount = layer.MapObjects.Count;
            if (pointsCount < MINIMUM_NEAREST_COUNT)
                return pointsCount;

            return (int) Math.Round(pointsCount * NEAREST_COUNT_PERCENT);
        }

        private static double CalculateRadius(VectorLayer layer)
        {
            if (layer == null)
                return 0;

            var bounds = layer.Bounds;

            var dx = bounds.XMax - bounds.XMin;
            var dy = bounds.YMax - bounds.YMin;
            var radius = Math.Sqrt(dx * dx + dy * dy) / 2.0;
            if (radius < MINIMUM_RADIUS)
                return radius;

            return (int) Math.Round(radius * RADIUS_PERCENT);
        }
    }
}
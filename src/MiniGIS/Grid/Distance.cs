﻿namespace MiniGIS.Grid
{
    using MiniGIS.Vector;

    public struct Distance
    {
        public GeoPoint Point;

        public double Dist;
    }
}
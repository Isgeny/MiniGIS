﻿namespace MiniGIS.Grid
{
    using System.Drawing;

    public class GeoColor
    {
        public GeoColor(double r, double g, double b)
        {
            R = r;
            G = g;
            B = b;
        }

        public GeoColor(Color color)
        {
            R = color.R;
            G = color.G;
            B = color.B;
        }

        public double B { get; set; }

        public double G { get; set; }

        public double R { get; set; }
    }
}
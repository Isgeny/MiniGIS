﻿namespace MiniGIS.Grid
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using MiniGIS.Vector;

    using Point = MiniGIS.Vector.Point;

    public class GridLayer : BaseLayer
    {
        private Bitmap _bitmap;

        private List<GridColor> _colors;

        private bool _needRecoloring;

        public GridLayer(GridGeometry geometry, string layerName) : base(layerName)
        {
            _needRecoloring = true;

            Bounds = GeoRect.Union(Bounds, new GeoRect(geometry.MinX, geometry.MaxX, geometry.MinY, geometry.MaxY));
            Geometry = geometry;
            Matrix = new GridMatrix(geometry.CountNodesY, geometry.CountNodesX);
        }

        public List<GridColor> Colors
        {
            get => _colors;
            set
            {
                _colors = value;
                _needRecoloring = true;
            }
        }

        public GridGeometry Geometry { get; }

        public double? this[int row, int column]
        {
            get => Matrix[row, column];
            set
            {
                Matrix[row, column] = value;
                _needRecoloring = true;
            }
        }

        public GridMatrix Matrix { get; set; }

        public Task CalculateAsync(GridRepairArea repairArea, IProgress<int> progress)
        {
            return Task.Run(() => Calculate(repairArea, progress));
        }

        public override void Draw(PaintEventArgs e)
        {
            if (!Visible)
                return;

            if (_bitmap == null)
                _bitmap = new Bitmap(Geometry.CountNodesX - 1, Geometry.CountNodesY - 1);

            if (_needRecoloring)
                Recolor();

            var gridTopLeft = Map.MapToScreen(Geometry.MinX, Geometry.MaxY);
            var gridBottomRight = Map.MapToScreen(Geometry.MaxX, Geometry.MinY);
            var gridSize = new Size(gridBottomRight.X - gridTopLeft.X, gridBottomRight.Y - gridTopLeft.Y);
            var gridArea = new Rectangle(gridTopLeft, gridSize);

            var g = e.Graphics;
            g.PixelOffsetMode = PixelOffsetMode.Half;
            g.InterpolationMode = InterpolationMode.NearestNeighbor;
            g.DrawImage(_bitmap, gridArea);
        }

        private void Calculate(GridRepairArea repairArea, IProgress<int> progress)
        {
            var points = repairArea.VectorLayer.MapObjects
                .Cast<Point>()
                .Select(p => p.Position)
                .ToArray();

            var countNodesX = Geometry.CountNodesX;
            var countNodesY = Geometry.CountNodesY;
            var minX = Geometry.MinX;
            var minY = Geometry.MinY;
            var step = Geometry.Step;
            var pointsSearchMethod = repairArea.PointsSearchMethod;
            var nearestCount = repairArea.NearestCount;
            var squareRadius = repairArea.SearchRadius * repairArea.SearchRadius;
            var pointsCount = points.Length;
            var negativePowerHalf = -repairArea.Power / 2.0;
            var distances = new Distance[pointsCount];

            var x = minX;
            for (var j = 0; j < countNodesX; j++)
            {
                var y = minY;
                for (var i = 0; i < countNodesY; i++)
                {
                    for (var k = 0; k < pointsCount; k++)
                    {
                        var point = points[k];
                        var dx = x - point.X;
                        var dy = y - point.Y;
                        distances[k].Point = point;
                        distances[k].Dist = dx * dx + dy * dy;
                    }

                    var filteredDistances = pointsSearchMethod == PointsSearchMethod.NearestCount
                        ? distances.OrderBy(d => d.Dist).Take(nearestCount)
                        : distances.Where(d => d.Dist <= squareRadius);

                    var numenator = 0.0;
                    var denominator = 0.0;
                    foreach (var distance in filteredDistances)
                    {
                        if (distance.Dist == 0.0)
                            continue;

                        var poweredDistance = Math.Pow(distance.Dist, negativePowerHalf);
                        numenator += distance.Point.Z * poweredDistance;
                        denominator += poweredDistance;
                    }

                    this[i, j] = denominator == 0.0 ? default(double?) : numenator / denominator;

                    y += step;
                }

                x += step;

                progress.Report(j * 100 / countNodesX);
            }
        }

        private void Recolor()
        {
            var minValue = Matrix.Min;
            var maxValue = Matrix.Max;
            if (minValue == null || maxValue == null)
                return;

            if (Colors == null)
                Colors = new List<GridColor>
                {
                    new GridColor { Color = Color.Blue, Value = minValue.Value },
                    new GridColor { Color = Color.Red, Value = maxValue.Value }
                };

            var interpolator = new ColorsInterpolator(Colors);

            for (var i = 0; i < Geometry.CountNodesY - 1; i++)
            for (var j = 0; j < Geometry.CountNodesX - 1; j++)
            {
                var feature = Matrix[Geometry.CountNodesY - i - 2, j];
                if (feature == null)
                {
                    _bitmap.SetPixel(j, i, Color.Transparent);
                    continue;
                }

                var color = interpolator.Interpolate(feature.Value);
                _bitmap.SetPixel(j, i, color);
            }

            _needRecoloring = false;
        }
    }
}
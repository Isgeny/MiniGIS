﻿namespace MiniGIS.Grid
{
    using System.Drawing;

    public class GridColor
    {
        public Color Color { get; set; }

        public double Value { get; set; }
    }
}
﻿namespace MiniGIS.Grid
{
    using System.Runtime.Serialization;

    public class GridMatrix
    {
        private double?[,] _matrix;

        private double? _max;

        private double? _min;

        private bool _needUpdateMax;

        private bool _needUpdateMin;

        public GridMatrix()
        {
        }

        public GridMatrix(int rows, int columns)
        {
            _matrix = new double?[rows, columns];
            _needUpdateMax = true;
            _needUpdateMin = true;
        }

        public double? this[int row, int column]
        {
            get => _matrix[row, column];
            set
            {
                _matrix[row, column] = value;
                _needUpdateMax = true;
                _needUpdateMin = true;
            }
        }

        public double?[,] Matrix
        {
            get => _matrix;
            set
            {
                _matrix = value;
                _needUpdateMax = true;
                _needUpdateMin = true;
            }
        }

        public double? Max
        {
            get
            {
                if (!_needUpdateMax)
                    return _max;

                foreach (var feature in _matrix)
                {
                    if (!_max.HasValue || feature.HasValue && feature.Value > _max)
                        _max = feature;
                }

                _needUpdateMax = false;
                return _max;
            }
        }

        public double? Min
        {
            get
            {
                if (!_needUpdateMin)
                    return _min;

                foreach (var feature in _matrix)
                {
                    if (!_min.HasValue || feature.HasValue && feature.Value < _min)
                        _min = feature;
                }

                _needUpdateMin = false;
                return _min;
            }
        }
    }
}
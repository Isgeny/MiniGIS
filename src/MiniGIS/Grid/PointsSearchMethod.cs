﻿namespace MiniGIS.Grid
{
    public enum PointsSearchMethod
    {
        NearestCount,

        Radius
    }
}
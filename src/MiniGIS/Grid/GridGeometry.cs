﻿namespace MiniGIS.Grid
{
    using System;

    using Newtonsoft.Json;

    public class GridGeometry
    {
        private const int DEFAULT_NODES_COUNT = 500;

        private int _countNodesX;

        private int _countNodesY;

        private double _maxX;

        private double _maxY;

        private double _minX;

        private double _minY;

        private double _step;

        public GridGeometry(double minX, double minY, double maxX, double maxY)
        {
            _minX = Math.Min(minX, maxX);
            _minY = Math.Min(minY, maxY);
            _maxX = Math.Max(maxX, minX);
            _maxY = Math.Max(maxY, minY);

            var dx = maxX - minX;
            var dy = maxY - minY;
            _step = Math.Max(dx, dy) / (DEFAULT_NODES_COUNT - 1.0);

            if (dx > dy)
            {
                CountNodesX = DEFAULT_NODES_COUNT;
                CountNodesY = CalculateCountNodesY(maxY, minY, _step);
            }
            else if (dx < dy)
            {
                CountNodesX = CalculateCountNodesX(maxX, minX, _step);
                CountNodesY = DEFAULT_NODES_COUNT;
            }
            else
            {
                CountNodesX = DEFAULT_NODES_COUNT;
                CountNodesY = DEFAULT_NODES_COUNT;
            }
        }

        [JsonIgnore]
        public int CountNodesX
        {
            get => _countNodesX;
            set
            {
                if (_countNodesX == value)
                    return;

                _countNodesX = value;
                _maxX = CalculateMaxX(_countNodesX, _minX, _step);
            }
        }

        [JsonIgnore]
        public int CountNodesY
        {
            get => _countNodesY;
            set
            {
                if (_countNodesY == value)
                    return;

                _countNodesY = value;
                _maxY = CalculateMaxY(_countNodesY, _minY, _step);
            }
        }

        public double MaxX
        {
            get => _maxX;
            set
            {
                if (_maxX == value)
                    return;

                _maxX = value;
                Step = CalculateStep(_maxX, _maxY, _minX, _minY, _countNodesX, _countNodesY);
            }
        }

        public double MaxY
        {
            get => _maxY;
            set
            {
                if (_maxY == value)
                    return;

                _maxY = value;
                Step = CalculateStep(_maxX, _maxY, _minX, _minY, _countNodesX, _countNodesY);
            }
        }

        public double MinX
        {
            get => _minX;
            set
            {
                if (_minX == value)
                    return;

                _minX = value;
                Step = CalculateStep(_maxX, _maxY, _minX, _minY, _countNodesX, _countNodesY);
            }
        }

        public double MinY
        {
            get => _minY;
            set
            {
                if (_minY == value)
                    return;

                _minY = value;
                Step = CalculateStep(_maxX, _maxY, _minX, _minY, _countNodesX, _countNodesY);
            }
        }

        public double Step
        {
            get => _step;
            set
            {
                if (_step == value)
                    return;

                _step = value;
                CountNodesX = CalculateCountNodesX(_maxX, _minX, _step);
                CountNodesY = CalculateCountNodesY(_maxY, _minY, _step);
            }
        }

        private static int CalculateCountNodesX(double maxX, double minX, double step)
        {
            return (int) Math.Ceiling((maxX - minX) / step + 1.0);
        }

        private static int CalculateCountNodesY(double maxY, double minY, double step)
        {
            return (int) Math.Ceiling((maxY - minY) / step + 1.0);
        }

        private static double CalculateMaxX(double countNodesX, double minX, double step)
        {
            return minX + step * (countNodesX - 1.0);
        }

        private static double CalculateMaxY(double countNodesY, double minY, double step)
        {
            return minY + step * (countNodesY - 1.0);
        }

        private static double CalculateStep(double maxX, double maxY, double minX, double minY,
            double countNodesX, double countNodexY)
        {
            return Math.Max(maxX - minX, maxY - minY) / (Math.Max(countNodesX, countNodexY) - 1.0);
        }
    }
}
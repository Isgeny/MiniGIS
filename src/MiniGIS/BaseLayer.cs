﻿namespace MiniGIS
{
    using System.Windows.Forms;

    using MiniGIS.Controls;
    using MiniGIS.Vector;

    using Newtonsoft.Json;

    public abstract class BaseLayer
    {
        protected BaseLayer()
        {
            Visible = true;
            Bounds = new GeoRect(0.0, 0.0, 0.0, 0.0);
        }

        protected BaseLayer(string name) : this()
        {
            Name = name;
        }

        public GeoRect Bounds { get; set; }

        [JsonIgnore]
        public Map Map { get; set; }

        public string Name { get; set; }

        public bool Visible { get; set; }

        public abstract void Draw(PaintEventArgs e);

        public override string ToString()
        {
            return Name;
        }
    }
}
﻿namespace MiniGIS
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

    using MiniGIS.Grid;
    using MiniGIS.Interfaces;

    public class ColorsInterpolator : IColorsInterpolator
    {
        private readonly List<GeoColor> _a0;

        private readonly List<GeoColor> _a1;

        private readonly List<GridColor> _colors;

        public ColorsInterpolator(List<GridColor> colors)
        {
            _colors = colors.OrderBy(c => c.Value).ToList();

            _a0 = new List<GeoColor> { new GeoColor(colors[0].Color) };
            _a1 = new List<GeoColor> { new GeoColor(colors[0].Color) };

            for (var i = 1; i < _colors.Count; i++)
            {
                var a1Value = GetA1(_colors[i].Value, _colors[i - 1].Value, _colors[i].Color, _colors[i - 1].Color);
                _a1.Add(a1Value);

                var a0Value = GetA0(_colors[i - 1].Value, _colors[i - 1].Color, a1Value);
                _a0.Add(a0Value);
            }
        }

        public Color Interpolate(double value)
        {
            for (var i = 0; i < _colors.Count - 1; i++)
            {
                if (_colors[i].Value > value || value > _colors[i + 1].Value)
                    continue;

                var r = (int) (_a0[i + 1].R + _a1[i + 1].R * value);
                var g = (int) (_a0[i + 1].G + _a1[i + 1].G * value);
                var b = (int) (_a0[i + 1].B + _a1[i + 1].B * value);
                return Color.FromArgb(r, g, b);
            }

            return Color.Transparent;
        }

        private static GeoColor GetA0(double prevX, Color prevY, GeoColor a1)
        {
            var r = prevY.R - a1.R * prevX;
            var g = prevY.G - a1.G * prevX;
            var b = prevY.B - a1.B * prevX;
            return new GeoColor(r, g, b);
        }

        private static GeoColor GetA1(double x, double prevX, Color y, Color prevY)
        {
            var dx = x - prevX;
            var r = (y.R - prevY.R) / dx;
            var g = (y.G - prevY.G) / dx;
            var b = (y.B - prevY.B) / dx;
            return new GeoColor(r, g, b);
        }
    }
}
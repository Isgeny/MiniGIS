﻿namespace MiniGIS
{
    using System;
    using System.IO;
    using System.Windows.Forms;

    using MiniGIS.Controls;

    public static class Program
    {
        [STAThread]
        private static void Main()
        {
            //var r = new Random(DateTime.Now.Millisecond);
            //using (var sw = new StreamWriter("Points125.txt"))
            //{
            //    for (var i = 0; i < 125; i++)
            //    {
            //        var x = r.Next(-100, 101);
            //        var y = r.Next(-50, 51);
            //        var z = r.Next(-10, 11);
            //        sw.WriteLine($"{x}\t{y}\t{z}");
            //    }
            //}


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
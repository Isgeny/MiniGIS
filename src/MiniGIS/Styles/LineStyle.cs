﻿namespace MiniGIS.Styles
{
    using System.Drawing;

    public class LineStyle
    {
        public LineStyle()
        {
            Color = Color.Black;
            Width = 1;
        }

        public LineStyle(Color color, float width)
        {
            Color = color;
            Width = width;
        }

        public Color Color { get; set; }

        public float Width { get; set; }
    }
}
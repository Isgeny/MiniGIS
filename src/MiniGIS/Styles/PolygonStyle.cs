﻿namespace MiniGIS.Styles
{
    using System.Drawing;

    public class PolygonStyle
    {
        public PolygonStyle()
        {
            Color = Color.Aqua;
        }

        public PolygonStyle(Color color)
        {
            Color = color;
        }

        public Color Color { get; set; }
    }
}
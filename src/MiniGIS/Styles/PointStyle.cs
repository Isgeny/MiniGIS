﻿namespace MiniGIS.Styles
{
    using System.Drawing;

    public class PointStyle
    {
        public PointStyle()
        {
            FontFamily = "Wingdings";
            Symbol = 0x9F;
            SymbolColor = Color.Black;
            SymbolSize = 7;
        }

        public PointStyle(string font, byte symbol, Color symbolColor, int symbolSize)
        {
            FontFamily = font;
            Symbol = symbol;
            SymbolColor = symbolColor;
            SymbolSize = symbolSize;
        }

        public string FontFamily { get; set; }

        public byte Symbol { get; set; }

        public Color SymbolColor { get; set; }

        public int SymbolSize { get; set; }
    }
}
﻿namespace MiniGIS.Interfaces
{
    public interface IInterpolator<TI, TO>
    {
        TO Interpolate(TI value);
    }
}
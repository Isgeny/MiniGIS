﻿namespace MiniGIS.Interfaces
{
    public interface ILayerRepositoriesFactory
    {
        ILayersRepository<BaseLayer> GetLayersRepository(string filePath);
    }
}
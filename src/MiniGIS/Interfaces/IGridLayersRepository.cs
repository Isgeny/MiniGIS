﻿namespace MiniGIS.Interfaces
{
    using MiniGIS.Grid;

    public interface IGridLayersRepository : ILayersRepository<GridLayer>
    {
        void CreateLayer(GridLayer layer);
    }
}
﻿namespace MiniGIS.Interfaces
{
    using System.Drawing;

    public interface IColorsInterpolator : IInterpolator<double, Color>
    {
    }
}
﻿namespace MiniGIS.Interfaces
{
    public interface IPointsInterpolator : IInterpolator<double, double>
    {
    }
}
﻿namespace MiniGIS.Interfaces
{
    using MiniGIS.Vector;

    public interface IVectorLayersRepository : ILayersRepository<VectorLayer>
    {
    }
}
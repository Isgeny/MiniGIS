﻿namespace MiniGIS.Interfaces
{
    public interface ILayersRepository<out T> where T : class
    {
        T GetLayer();
    }
}
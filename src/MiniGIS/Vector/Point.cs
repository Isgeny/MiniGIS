﻿namespace MiniGIS.Vector
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    using MiniGIS.Styles;

    public class Point : MapObject
    {
        private readonly PointStyle _style;

        private Graphics _graphics;

        public Point(GeoPoint position)
        {
            Position = position;
            _style = new PointStyle();
        }

        public Point(GeoPoint position, PointStyle style)
        {
            Position = position;
            _style = style;
        }

        public GeoPoint Position { get; set; }

        public override void Draw(PaintEventArgs e)
        {
            if (_graphics == null)
                _graphics = e.Graphics;

            float symbolSize;
            if (Layer.PointsSizeMethod == PointsSizeMethod.FixedSize)
                symbolSize = (float) Layer.FixedSymbolSize;
            else
                symbolSize = (float) Layer.PointsInterpolator.Interpolate(Position.Z);

            var font = new Font(_style.FontFamily, symbolSize);

            var brush = new SolidBrush(_style.SymbolColor);
            var symbol = Convert.ToChar(_style.Symbol).ToString();
            var graphics = e.Graphics;
            var symbolFontSize = graphics.MeasureString(symbol, font);

            // Отрисовываем символ с центром в screenLocation
            var screenLocaltion = Layer.Map.MapToScreen(Position);
            screenLocaltion.X -= (int) (symbolFontSize.Width / 2.0f);
            screenLocaltion.Y -= (int) (symbolFontSize.Height / 2.0f);

            if (Selected)
            {
                var pen = new Pen(Color.Black, 1.0f) { DashPattern = new[] { 4.0f, 2.0f } };
                var rect = new Rectangle(screenLocaltion, symbolFontSize.ToSize());
                graphics.DrawRectangle(pen, rect);
            }

            graphics.DrawString(symbol, font, brush, screenLocaltion);
        }

        public override GeoRect GetBounds()
        {
            return new GeoRect(Position.X, Position.X, Position.Y, Position.Y);
        }

        public override bool IsInside(GeoRect geoRect)
        {
            var graphics = Layer.Map.CreateGraphics();
            var font = new Font(_style.FontFamily, _style.SymbolSize);
            var symbol = Convert.ToChar(_style.Symbol).ToString();
            var symbolSize = graphics.MeasureString(symbol, font);

            var topLeft = Layer.Map.MapToScreen(Position);
            topLeft.X -= (int) (symbolSize.Width / 2);
            topLeft.Y -= (int) (symbolSize.Height / 2);
            var bottomRight = Layer.Map.MapToScreen(Position);
            bottomRight.X -= (int) (symbolSize.Width / 2);
            bottomRight.X += (int) symbolSize.Width;
            bottomRight.Y -= (int) (symbolSize.Height / 2);
            bottomRight.Y += (int) symbolSize.Height;

            var topLeftLocation = Layer.Map.ScreenToMap(topLeft);
            var bottomRightLocation = Layer.Map.ScreenToMap(bottomRight);

            var pointRect = new GeoRect(topLeftLocation.X, bottomRightLocation.X, bottomRightLocation.Y,
                topLeftLocation.Y);
            return GeoRect.IsIntersect(pointRect, geoRect);
        }

        public override string ToString()
        {
            return $"{Position.X} {Position.Y} {Position.Z}";
        }
    }
}
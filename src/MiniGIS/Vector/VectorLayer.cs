﻿namespace MiniGIS.Vector
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    public class VectorLayer : BaseLayer
    {
        public VectorLayer(string name) : base(name)
        {
            MapObjects = new List<MapObject>();
            PointsInterpolator = new LinearInterpolator();
            FixedSymbolSize = 8;
        }

        public double FixedSymbolSize { get; set; }

        public List<MapObject> MapObjects { get; }

        public LinearInterpolator PointsInterpolator { get; }

        public PointsSizeMethod PointsSizeMethod { get; set; }

        public void AddMapObject(MapObject mapObject)
        {
            mapObject.Layer = this;
            MapObjects.Add(mapObject);
            Bounds = GeoRect.Union(Bounds, mapObject.GeoBounds);

            var points = MapObjects.OfType<Point>().ToList();
            PointsInterpolator.X1 = points.Min(p => p.Position.Z);
            PointsInterpolator.X2 = points.Max(p => p.Position.Z);
        }

        public override void Draw(PaintEventArgs e)
        {
            if (!Visible)
                return;

            MapObjects.ForEach(o => o.Draw(e));
        }
    }
}
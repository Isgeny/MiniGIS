﻿namespace MiniGIS.Vector
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    using MiniGIS.Styles;

    public class PolyLine : MapObject
    {
        public PolyLine()
        {
            Nodes = new List<GeoPoint>();
            LineStyle = new LineStyle();
        }

        public PolyLine(List<GeoPoint> nodes, LineStyle style)
        {
            var nodesCount = nodes.Count;
            Nodes = new List<GeoPoint>(nodesCount);
            foreach (var node in nodes)
            {
                var point = new GeoPoint(node.X, node.Y);
                Nodes.Add(point);
            }
            LineStyle = style;
        }

        public LineStyle LineStyle { get; set; }

        public List<GeoPoint> Nodes { get; set; }

        public void AddNode(GeoPoint point)
        {
            Nodes.Add(point);
        }

        public int CountNodes()
        {
            return Nodes.Count;
        }

        public override void Draw(PaintEventArgs e)
        {
            var graphics = e.Graphics;

            var lineWidth = LineStyle.Width;
            var lineColor = LineStyle.Color;
            var pen = new Pen(lineColor, lineWidth);
            if (Selected)
                pen.DashPattern = new[] { 4.0f, 2.0f };

            var points = new System.Drawing.Point[CountNodes()];
            for (var i = 0; i < CountNodes(); ++i)
                points[i] = Layer.Map.MapToScreen(Nodes[i]);

            graphics.DrawLines(pen, points);
        }

        public override GeoRect GetBounds()
        {
            double xMin = Nodes[0].X, xMax = Nodes[0].X, yMin = Nodes[0].Y, yMax = Nodes[0].Y;
            foreach (var node in Nodes)
            {
                xMin = Math.Min(xMin, node.X);
                xMax = Math.Max(xMax, node.X);
                yMin = Math.Min(yMin, node.Y);
                yMax = Math.Max(yMax, node.Y);
            }
            return new GeoRect(xMin, xMax, yMin, yMax);
        }

        public override bool IsInside(GeoRect geoRect)
        {
            for (var i = 0; i < CountNodes() - 1; ++i)
            {
                var line = new Line
                {
                    BeginPoint = Nodes[i],
                    EndPoint = Nodes[i + 1]
                };

                if (GeoRect.IsCrossRectLines(geoRect, line))
                    return true;
            }
            return false;
        }
    }
}
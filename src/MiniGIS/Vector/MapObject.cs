﻿namespace MiniGIS.Vector
{
    using System.Windows.Forms;

    public abstract class MapObject
    {
        // Прямоугольная область, в которую вписан данный объект
        public GeoRect GeoBounds => GetBounds();

        // Слой, в котором находится данный объект
        public VectorLayer Layer { get; set; }

        // Выделен ли объект (рисуется пунктирный контур)
        public bool Selected { get; set; }

        // Рисование объекта
        public abstract void Draw(PaintEventArgs e);

        // Получение прямоугольной области
        public abstract GeoRect GetBounds();

        // Проверка вхождения прямоугольника в прямоугольник текущего объекта
        public abstract bool IsInside(GeoRect geoRect);
    }
}
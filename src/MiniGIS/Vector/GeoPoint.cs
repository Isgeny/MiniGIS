﻿namespace MiniGIS.Vector
{
    public class GeoPoint
    {
        public GeoPoint()
        {
            X = 0.0;
            Y = 0.0;
            Z = 0.0;
        }

        public GeoPoint(double x, double y) : this()
        {
            X = x;
            Y = y;
        }

        public GeoPoint(double x, double y, double z) : this(x, y)
        {
            Z = z;
        }

        public double X { get; set; }

        public double Y { get; set; }

        public double Z { get; set; }

        public override string ToString()
        {
            return $"{X} {Y} {Z}";
        }
    }
}
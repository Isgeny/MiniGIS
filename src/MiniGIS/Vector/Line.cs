﻿namespace MiniGIS.Vector
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    using MiniGIS.Styles;

    public class Line : MapObject
    {
        public Line()
        {
            BeginPoint = new GeoPoint();
            EndPoint = new GeoPoint();
            Style = new LineStyle();
        }

        public Line(GeoPoint beginPoint, GeoPoint endPoint, LineStyle style)
        {
            BeginPoint = beginPoint;
            EndPoint = endPoint;
            Style = style;
        }

        public GeoPoint BeginPoint { get; set; }

        public GeoPoint EndPoint { get; set; }

        public LineStyle Style { get; set; }

        public override void Draw(PaintEventArgs e)
        {
            var graphics = e.Graphics;
            var screenLocaltionBeginPoint = Layer.Map.MapToScreen(BeginPoint);
            var screenLocaltionEndPoint = Layer.Map.MapToScreen(EndPoint);

            var lineWidth = Style.Width;
            var lineColor = Style.Color;
            var pen = new Pen(lineColor, lineWidth);
            if (Selected)
                pen.DashPattern = new[] { 4.0f, 2.0f };

            graphics.DrawLine(pen, screenLocaltionBeginPoint, screenLocaltionEndPoint);
        }

        public override GeoRect GetBounds()
        {
            var xMin = Math.Min(BeginPoint.X, EndPoint.X);
            var xMax = Math.Max(BeginPoint.X, EndPoint.X);
            var yMin = Math.Min(BeginPoint.Y, EndPoint.Y);
            var yMax = Math.Max(BeginPoint.Y, EndPoint.Y);
            return new GeoRect(xMin, xMax, yMin, yMax);
        }

        public static bool IsCrossLines(Line line1, Line line2)
        {
            var v1 = (line1.EndPoint.X - line1.BeginPoint.X) * (line2.BeginPoint.Y - line1.BeginPoint.Y) -
                     (line1.EndPoint.Y - line1.BeginPoint.Y) * (line2.BeginPoint.X - line1.BeginPoint.X);
            var v2 = (line1.EndPoint.X - line1.BeginPoint.X) * (line2.EndPoint.Y - line1.BeginPoint.Y) -
                     (line1.EndPoint.Y - line1.BeginPoint.Y) * (line2.EndPoint.X - line1.BeginPoint.X);
            var v3 = (line2.EndPoint.X - line2.BeginPoint.X) * (line1.BeginPoint.Y - line2.BeginPoint.Y) -
                     (line2.EndPoint.Y - line2.BeginPoint.Y) * (line1.BeginPoint.X - line2.BeginPoint.X);
            var v4 = (line2.EndPoint.X - line2.BeginPoint.X) * (line1.EndPoint.Y - line2.BeginPoint.Y) -
                     (line2.EndPoint.Y - line2.BeginPoint.Y) * (line1.EndPoint.X - line2.BeginPoint.X);
            return v1 * v2 < 0 && v3 * v4 < 0;
        }

        public override bool IsInside(GeoRect geoRect)
        {
            return GeoRect.IsCrossRectLines(geoRect, this);
        }
    }
}
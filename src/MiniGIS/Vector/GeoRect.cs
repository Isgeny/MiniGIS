﻿namespace MiniGIS.Vector
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class GeoRect
    {
        public GeoRect(double xMin, double xMax, double yMin, double yMax)
        {
            XMin = xMin;
            XMax = xMax;
            YMin = yMin;
            YMax = yMax;
        }

        public double XMax { get; set; }

        public double XMin { get; set; }

        public double YMax { get; set; }

        public double YMin { get; set; }

        // Находится ли точка внутри прямоугольника
        public static bool Contains(GeoRect rect, GeoPoint point)
        {
            return rect.XMin < point.X && rect.XMax > point.X && rect.YMin < point.Y && rect.YMax > point.Y;
        }

        // Находится ли полностью прямоугольник rect1 внутри прямоугольника rect2
        public static bool Contains(GeoRect rect1, GeoRect rect2)
        {
            return rect2.XMin < rect1.XMin && rect2.XMax > rect1.XMax && rect2.YMin < rect1.YMin &&
                   rect2.YMax > rect1.YMax;
        }

        // Преобразует прямоугольник в список из 4х отрезков - его сторон
        public static List<Line> GEORectToLines(GeoRect geoRect)
        {
            var topLine = new Line
            {
                BeginPoint = new GeoPoint(geoRect.XMin, geoRect.YMax),
                EndPoint = new GeoPoint(geoRect.XMax, geoRect.YMax)
            };

            var rightLine = new Line
            {
                BeginPoint = new GeoPoint(geoRect.XMax, geoRect.YMax),
                EndPoint = new GeoPoint(geoRect.XMax, geoRect.YMin)
            };

            var bottomLine = new Line
            {
                BeginPoint = new GeoPoint(geoRect.XMax, geoRect.YMin),
                EndPoint = new GeoPoint(geoRect.XMin, geoRect.YMin)
            };

            var leftLine = new Line
            {
                BeginPoint = new GeoPoint(geoRect.XMin, geoRect.YMin),
                EndPoint = new GeoPoint(geoRect.XMin, geoRect.YMax)
            };

            var lines = new List<Line> { topLine, rightLine, bottomLine, leftLine };
            return lines;
        }

        // Пересекается ли прямоугольник с отрезком
        public static bool IsCrossRectLines(GeoRect geoRect, Line line)
        {
            // Разбиваем прямоугольник на 4 отрезка
            var rectLines = GEORectToLines(geoRect);

            // Проверяем если точка отрезка находится внутри прямоугольника
            // Проверяем пересечение каждой стороны прямоугольника с исходным отрезком
            return Contains(geoRect, line.BeginPoint) || rectLines.Any(rectLine => Line.IsCrossLines(line, rectLine));
        }

        // Существует ли такой прямоугольник
        public static bool IsExist(GeoRect geoRect)
        {
            return geoRect.XMin != 0.0 || geoRect.XMax != 0.0 || geoRect.YMin != 0.0 || geoRect.YMax != 0.0;
        }

        // Пересекаются ли два прямоугольника
        public static bool IsIntersect(GeoRect geoRect1, GeoRect geoRect2)
        {
            return geoRect1.YMin < geoRect2.YMax && geoRect1.YMax > geoRect2.YMin && geoRect1.XMax > geoRect2.XMin &&
                   geoRect1.XMin < geoRect2.XMax;
        }

        // Объединение двух прямоугольник в один большой прямоугольник
        public static GeoRect Union(GeoRect geoRect1, GeoRect geoRect2)
        {
            if (!IsExist(geoRect1))
                return geoRect2;

            if (!IsExist(geoRect2))
                return geoRect1;

            if (!IsExist(geoRect1) && !IsExist(geoRect2))
                return new GeoRect(0.0, 0.0, 0.0, 0.0);

            var xMin = Math.Min(geoRect1.XMin, geoRect2.XMin);
            var xMax = Math.Max(geoRect1.XMax, geoRect2.XMax);
            var yMin = Math.Min(geoRect1.YMin, geoRect2.YMin);
            var yMax = Math.Max(geoRect1.YMax, geoRect2.YMax);
            return new GeoRect(xMin, xMax, yMin, yMax);
        }
    }
}
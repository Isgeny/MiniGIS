﻿namespace MiniGIS.Vector
{
    public enum PointsSizeMethod
    {
        FixedSize,
        ProportionalSize
    }
}
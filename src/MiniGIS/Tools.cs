﻿namespace MiniGIS
{
    public enum Tools
    {
        Pan,
        ZoomIn,
        ZoomOut
    }
}
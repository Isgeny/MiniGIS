﻿namespace MiniGIS.Controls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Forms;

    using MiniGIS.Grid;
    using MiniGIS.Vector;

    public partial class RepairGridForm : Form
    {
        private GridGeometry _geometry;

        private GridRepairArea _repairArea;

        public RepairGridForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowForCalc(List<VectorLayer> pointLayers, VectorLayer selectedLayer,
            out GridRepairArea repairArea, out GridGeometry geometry)
        {
            repairArea = null;
            geometry = null;

            if (pointLayers?.Any() != true)
                return DialogResult.Abort;

            _repairArea = new GridRepairArea(selectedLayer);

            comboBoxLayers.Items.AddRange(pointLayers.ToArray());
            comboBoxLayers.SelectedItem = selectedLayer;

            var dialogResult = ShowDialog();
            if (dialogResult != DialogResult.OK)
                return dialogResult;

            _repairArea.PointsSearchMethod = MapRadioPointsSearchMethodToEnum();

            repairArea = _repairArea;
            geometry = _geometry;
            return dialogResult;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void comboBoxLayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!(comboBoxLayers.SelectedItem is VectorLayer selectedLayer))
                return;

            var bounds = selectedLayer.Bounds;
            _geometry = new GridGeometry(bounds.XMin, bounds.YMin, bounds.XMax, bounds.YMax);
            UpdateFormGeometry();

            _repairArea.VectorLayer = selectedLayer;
            UpdateFormRepairArea();
        }

        private PointsSearchMethod MapRadioPointsSearchMethodToEnum()
        {
            return radioBtnCount.Checked ? PointsSearchMethod.NearestCount : PointsSearchMethod.Radius;
        }

        private void numericUpDownCount_Validated(object sender, EventArgs e)
        {
            _repairArea.NearestCount = Convert.ToInt32(numericUpDownCount.Value);
        }

        private void numericUpDownMaxX_Validated(object sender, EventArgs e)
        {
            _geometry.MaxX = Convert.ToDouble(numericUpDownMaxX.Value);
            UpdateFormGeometry();
        }

        private void numericUpDownMaxY_Validated(object sender, EventArgs e)
        {
            _geometry.MaxY = Convert.ToDouble(numericUpDownMaxY.Value);
            UpdateFormGeometry();
        }

        private void numericUpDownMinX_Validated(object sender, EventArgs e)
        {
            _geometry.MinX = Convert.ToDouble(numericUpDownMinX.Value);
            UpdateFormGeometry();
        }

        private void numericUpDownMinY_Validated(object sender, EventArgs e)
        {
            _geometry.MinY = Convert.ToDouble(numericUpDownMinY.Value);
            UpdateFormGeometry();
        }

        private void numericUpDownPower_Validated(object sender, EventArgs e)
        {
            _repairArea.Power = Convert.ToInt32(numericUpDownPower.Value);
            UpdateFormRepairArea();
        }

        private void numericUpDownRadius_Validated(object sender, EventArgs e)
        {
            _repairArea.SearchRadius = Convert.ToDouble(numericUpDownRadius.Value);
            UpdateFormRepairArea();
        }

        private void numericUpDownStep_Validated(object sender, EventArgs e)
        {
            _geometry.Step = Convert.ToDouble(numericUpDownStep.Value);
            UpdateFormGeometry();
        }

        private void radioBtnCount_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownCount.Enabled = true;
            numericUpDownRadius.Enabled = false;
        }

        private void radioBtnRadius_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownCount.Enabled = false;
            numericUpDownRadius.Enabled = true;
        }

        private void UpdateFormGeometry()
        {
            numericUpDownMinX.Text = _geometry.MinX.ToString();
            numericUpDownMaxX.Text = _geometry.MaxX.ToString();
            numericUpDownMaxY.Text = _geometry.MaxY.ToString();
            numericUpDownMinY.Text = _geometry.MinY.ToString();
            numericUpDownStep.Text = _geometry.Step.ToString();
            txtBoxPointsX.Text = _geometry.CountNodesX.ToString();
            txtBoxPointsY.Text = _geometry.CountNodesY.ToString();
        }

        private void UpdateFormRepairArea()
        {
            numericUpDownCount.Text = _repairArea.NearestCount.ToString();
            numericUpDownRadius.Text = _repairArea.SearchRadius.ToString();
            numericUpDownPower.Text = _repairArea.Power.ToString();
        }

        private void ValidatingX(object sender, CancelEventArgs e)
        {
            if (numericUpDownMinX.Value > numericUpDownMaxX.Value)
                e.Cancel = true;
        }

        private void ValidatingY(object sender, CancelEventArgs e)
        {
            if (numericUpDownMinY.Value > numericUpDownMaxY.Value)
                e.Cancel = true;
        }
    }
}
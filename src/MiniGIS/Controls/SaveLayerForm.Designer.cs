﻿namespace MiniGIS.Controls
{
    partial class SaveLayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaveLayerForm));
            this._btnOk = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this._comboBoxLayers = new System.Windows.Forms.ComboBox();
            this._lblLayer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _btnOk
            // 
            this._btnOk.Location = new System.Drawing.Point(79, 49);
            this._btnOk.Margin = new System.Windows.Forms.Padding(2);
            this._btnOk.Name = "_btnOk";
            this._btnOk.Size = new System.Drawing.Size(56, 20);
            this._btnOk.TabIndex = 10;
            this._btnOk.Text = "ОК";
            this._btnOk.UseVisualStyleBackColor = true;
            this._btnOk.Click += new System.EventHandler(this._btnOk_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.Location = new System.Drawing.Point(139, 49);
            this._btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(56, 20);
            this._btnCancel.TabIndex = 9;
            this._btnCancel.Text = "Отмена";
            this._btnCancel.UseVisualStyleBackColor = true;
            this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
            // 
            // _comboBoxLayers
            // 
            this._comboBoxLayers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxLayers.FormattingEnabled = true;
            this._comboBoxLayers.Location = new System.Drawing.Point(46, 13);
            this._comboBoxLayers.Margin = new System.Windows.Forms.Padding(2);
            this._comboBoxLayers.Name = "_comboBoxLayers";
            this._comboBoxLayers.Size = new System.Drawing.Size(149, 21);
            this._comboBoxLayers.TabIndex = 12;
            // 
            // _lblLayer
            // 
            this._lblLayer.AutoSize = true;
            this._lblLayer.Location = new System.Drawing.Point(10, 16);
            this._lblLayer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._lblLayer.Name = "_lblLayer";
            this._lblLayer.Size = new System.Drawing.Size(32, 13);
            this._lblLayer.TabIndex = 11;
            this._lblLayer.Text = "Слой";
            // 
            // SaveLayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 80);
            this.Controls.Add(this._comboBoxLayers);
            this.Controls.Add(this._lblLayer);
            this.Controls.Add(this._btnOk);
            this.Controls.Add(this._btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveLayerForm";
            this.Text = "Сохранение слоя";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _btnOk;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.ComboBox _comboBoxLayers;
        private System.Windows.Forms.Label _lblLayer;
    }
}
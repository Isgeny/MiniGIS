﻿namespace MiniGIS.Controls
{
    partial class GridStyleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GridStyleForm));
            this._colorsGroupBox = new System.Windows.Forms.GroupBox();
            this._colorsDataGrid = new System.Windows.Forms.DataGridView();
            this._colorsDataSource = new System.Windows.Forms.BindingSource(this.components);
            this._btnOk = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this.Color = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._colorsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._colorsDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorsDataSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _colorsGroupBox
            // 
            this._colorsGroupBox.Controls.Add(this._colorsDataGrid);
            this._colorsGroupBox.Location = new System.Drawing.Point(16, 15);
            this._colorsGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._colorsGroupBox.Name = "_colorsGroupBox";
            this._colorsGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._colorsGroupBox.Size = new System.Drawing.Size(305, 215);
            this._colorsGroupBox.TabIndex = 6;
            this._colorsGroupBox.TabStop = false;
            this._colorsGroupBox.Text = "Цветовая схема";
            // 
            // _colorsDataGrid
            // 
            this._colorsDataGrid.AllowUserToResizeColumns = false;
            this._colorsDataGrid.AllowUserToResizeRows = false;
            this._colorsDataGrid.AutoGenerateColumns = false;
            this._colorsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._colorsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Color,
            this.Value});
            this._colorsDataGrid.DataSource = this._colorsDataSource;
            this._colorsDataGrid.Location = new System.Drawing.Point(8, 23);
            this._colorsDataGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._colorsDataGrid.MultiSelect = false;
            this._colorsDataGrid.Name = "_colorsDataGrid";
            this._colorsDataGrid.RowHeadersVisible = false;
            this._colorsDataGrid.RowHeadersWidth = 85;
            this._colorsDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._colorsDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._colorsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this._colorsDataGrid.ShowCellErrors = false;
            this._colorsDataGrid.ShowCellToolTips = false;
            this._colorsDataGrid.ShowEditingIcon = false;
            this._colorsDataGrid.ShowRowErrors = false;
            this._colorsDataGrid.Size = new System.Drawing.Size(289, 185);
            this._colorsDataGrid.TabIndex = 0;
            this._colorsDataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._colorsDataGrid_CellClick);
            this._colorsDataGrid.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this._colorsDataGrid_RowsAdded);
            // 
            // _btnOk
            // 
            this._btnOk.Location = new System.Drawing.Point(167, 236);
            this._btnOk.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._btnOk.Name = "_btnOk";
            this._btnOk.Size = new System.Drawing.Size(75, 25);
            this._btnOk.TabIndex = 8;
            this._btnOk.Text = "ОК";
            this._btnOk.UseVisualStyleBackColor = true;
            this._btnOk.Click += new System.EventHandler(this._btnOk_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.Location = new System.Drawing.Point(247, 236);
            this._btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(75, 25);
            this._btnCancel.TabIndex = 7;
            this._btnCancel.Text = "Отмена";
            this._btnCancel.UseVisualStyleBackColor = true;
            this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
            // 
            // Color
            // 
            this.Color.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Color.DataPropertyName = "Color";
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Transparent;
            this.Color.DefaultCellStyle = dataGridViewCellStyle1;
            this.Color.HeaderText = "Цвет";
            this.Color.MaxInputLength = 0;
            this.Color.Name = "Color";
            this.Color.ReadOnly = true;
            this.Color.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Color.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Value.DataPropertyName = "Value";
            dataGridViewCellStyle2.Format = "N0";
            this.Value.DefaultCellStyle = dataGridViewCellStyle2;
            this.Value.HeaderText = "Значение";
            this.Value.MaxInputLength = 30;
            this.Value.Name = "Value";
            this.Value.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // GridStyleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 268);
            this.Controls.Add(this._btnOk);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this._colorsGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GridStyleForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Стиль регулярной сети";
            this._colorsGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._colorsDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorsDataSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _colorsGroupBox;
        private System.Windows.Forms.DataGridView _colorsDataGrid;
        private System.Windows.Forms.BindingSource _colorsDataSource;
        private System.Windows.Forms.Button _btnOk;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Color;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
    }
}
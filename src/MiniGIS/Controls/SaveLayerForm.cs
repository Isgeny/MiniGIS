﻿namespace MiniGIS.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using MiniGIS.Grid;
    using MiniGIS.Interfaces;

    public partial class SaveLayerForm : Form
    {
        private readonly IGridLayersRepository _gridLayersRepository;

        public SaveLayerForm(IGridLayersRepository gridLayersRepository)
        {
            InitializeComponent();

            _gridLayersRepository = gridLayersRepository;
        }

        public DialogResult ShowForSave(List<GridLayer> layers, GridLayer selectedLayer)
        {
            _comboBoxLayers.Items.AddRange(layers.ToArray());
            _comboBoxLayers.SelectedItem = selectedLayer;

            var dialogResult = ShowDialog();
            if (dialogResult != DialogResult.OK)
                return dialogResult;

            _gridLayersRepository.CreateLayer((GridLayer) _comboBoxLayers.SelectedItem);

            return dialogResult;
        }

        private void _btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void _btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void _btnOpenFileDialog_Click(object sender, EventArgs e)
        {
            var fileDialog = new SaveFileDialog();
        }
    }
}
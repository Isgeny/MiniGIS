namespace MiniGIS.Controls
{
    using MiniGIS.Vector;

    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            MiniGIS.Vector.GeoPoint geoPoint1 = new MiniGIS.Vector.GeoPoint();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.openLayerBtn = new System.Windows.Forms.ToolStripButton();
            this.panBtn = new System.Windows.Forms.ToolStripButton();
            this.zoomInBtn = new System.Windows.Forms.ToolStripButton();
            this.zoomOutBtn = new System.Windows.Forms.ToolStripButton();
            this.entireViewBtn = new System.Windows.Forms.ToolStripButton();
            this._repairGridBtn = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._timeStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this._mapStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.listViewLayers = new System.Windows.Forms.ListView();
            this.groupBoxLayers = new System.Windows.Forms.GroupBox();
            this.groupBoxMap = new System.Windows.Forms.GroupBox();
            this.map = new MiniGIS.Controls.Map();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this._layerContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._saveAsLayer = new System.Windows.Forms.ToolStripMenuItem();
            this._editMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.groupBoxLayers.SuspendLayout();
            this.groupBoxMap.SuspendLayout();
            this._layerContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openLayerBtn,
            this.panBtn,
            this.zoomInBtn,
            this.zoomOutBtn,
            this.entireViewBtn,
            this._repairGridBtn});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(156, 27);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip";
            // 
            // openLayerBtn
            // 
            this.openLayerBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openLayerBtn.Image = global::MiniGIS.Properties.Resources.OpenLayer;
            this.openLayerBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openLayerBtn.Name = "openLayerBtn";
            this.openLayerBtn.Size = new System.Drawing.Size(24, 24);
            this.openLayerBtn.Text = "OpenLayer";
            this.openLayerBtn.ToolTipText = "Открыть слой";
            this.openLayerBtn.Click += new System.EventHandler(this.OpenLayerHandler);
            // 
            // panBtn
            // 
            this.panBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.panBtn.Image = global::MiniGIS.Properties.Resources.Pan;
            this.panBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.panBtn.Name = "panBtn";
            this.panBtn.Size = new System.Drawing.Size(24, 24);
            this.panBtn.Text = "Pan";
            this.panBtn.Click += new System.EventHandler(this.ToolStripButtonClickHandler);
            // 
            // zoomInBtn
            // 
            this.zoomInBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomInBtn.Image = global::MiniGIS.Properties.Resources.ZoomIn;
            this.zoomInBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomInBtn.Name = "zoomInBtn";
            this.zoomInBtn.Size = new System.Drawing.Size(24, 24);
            this.zoomInBtn.Text = "ZoomIn";
            this.zoomInBtn.ToolTipText = "Zoom In";
            this.zoomInBtn.Click += new System.EventHandler(this.ToolStripButtonClickHandler);
            // 
            // zoomOutBtn
            // 
            this.zoomOutBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomOutBtn.Image = global::MiniGIS.Properties.Resources.ZoomOut;
            this.zoomOutBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomOutBtn.Name = "zoomOutBtn";
            this.zoomOutBtn.Size = new System.Drawing.Size(24, 24);
            this.zoomOutBtn.Text = "ZoomOut";
            this.zoomOutBtn.ToolTipText = "Zoom Out";
            this.zoomOutBtn.Click += new System.EventHandler(this.ToolStripButtonClickHandler);
            // 
            // entireViewBtn
            // 
            this.entireViewBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.entireViewBtn.Image = global::MiniGIS.Properties.Resources.EntireView;
            this.entireViewBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.entireViewBtn.Name = "entireViewBtn";
            this.entireViewBtn.Size = new System.Drawing.Size(24, 24);
            this.entireViewBtn.Text = "EntireView";
            this.entireViewBtn.ToolTipText = "Entire View";
            this.entireViewBtn.Click += new System.EventHandler(this.EntireView);
            // 
            // _repairGridBtn
            // 
            this._repairGridBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._repairGridBtn.Image = global::MiniGIS.Properties.Resources.Grid;
            this._repairGridBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._repairGridBtn.Name = "_repairGridBtn";
            this._repairGridBtn.Size = new System.Drawing.Size(24, 24);
            this._repairGridBtn.Text = "Grid";
            this._repairGridBtn.Click += new System.EventHandler(this.RepairGridHandler);
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._progressBar,
            this._timeStatus,
            this._mapStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 616);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.statusStrip.Size = new System.Drawing.Size(716, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // _progressBar
            // 
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _timeStatus
            // 
            this._timeStatus.Name = "_timeStatus";
            this._timeStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // _mapStatus
            // 
            this._mapStatus.Name = "_mapStatus";
            this._mapStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // listViewLayers
            // 
            this.listViewLayers.AllowDrop = true;
            this.listViewLayers.CheckBoxes = true;
            this.listViewLayers.Location = new System.Drawing.Point(4, 17);
            this.listViewLayers.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listViewLayers.MultiSelect = false;
            this.listViewLayers.Name = "listViewLayers";
            this.listViewLayers.Size = new System.Drawing.Size(131, 230);
            this.listViewLayers.TabIndex = 3;
            this.listViewLayers.UseCompatibleStateImageBehavior = false;
            this.listViewLayers.View = System.Windows.Forms.View.List;
            this.listViewLayers.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewLayers_ItemChecked);
            this.listViewLayers.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.listViewLayers_ItemDrag);
            this.listViewLayers.SelectedIndexChanged += new System.EventHandler(this.listViewLayers_SelectedIndexChanged);
            this.listViewLayers.DragEnter += new System.Windows.Forms.DragEventHandler(this.listViewLayers_DragEnter);
            this.listViewLayers.DragOver += new System.Windows.Forms.DragEventHandler(this.listViewLayers_DragOver);
            this.listViewLayers.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listViewLayers_KeyUp);
            this.listViewLayers.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listViewLayers_MouseClick);
            // 
            // groupBoxLayers
            // 
            this.groupBoxLayers.Controls.Add(this.listViewLayers);
            this.groupBoxLayers.Location = new System.Drawing.Point(9, 29);
            this.groupBoxLayers.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxLayers.Name = "groupBoxLayers";
            this.groupBoxLayers.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxLayers.Size = new System.Drawing.Size(139, 251);
            this.groupBoxLayers.TabIndex = 12;
            this.groupBoxLayers.TabStop = false;
            this.groupBoxLayers.Text = "Слои";
            // 
            // groupBoxMap
            // 
            this.groupBoxMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMap.Controls.Add(this.map);
            this.groupBoxMap.Location = new System.Drawing.Point(152, 29);
            this.groupBoxMap.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxMap.Name = "groupBoxMap";
            this.groupBoxMap.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxMap.Size = new System.Drawing.Size(553, 574);
            this.groupBoxMap.TabIndex = 13;
            this.groupBoxMap.TabStop = false;
            this.groupBoxMap.Text = "Карта";
            // 
            // map
            // 
            this.map.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.map.AutoSize = true;
            this.map.CurrentTools = MiniGIS.Tools.Pan;
            this.map.Location = new System.Drawing.Point(4, 17);
            geoPoint1.X = 0D;
            geoPoint1.Y = 0D;
            geoPoint1.Z = 0D;
            this.map.MapCenter = geoPoint1;
            this.map.MapScale = 1D;
            this.map.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.map.Name = "map";
            this.map.SelectedLayer = null;
            this.map.Size = new System.Drawing.Size(544, 553);
            this.map.TabIndex = 0;
            this.map.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapControl_MouseMove);
            // 
            // _layerContextMenu
            // 
            this._layerContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this._layerContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._saveAsLayer,
            this._editMenuItem});
            this._layerContextMenu.Name = "_editContextMenu";
            this._layerContextMenu.Size = new System.Drawing.Size(189, 70);
            // 
            // _saveAsLayer
            // 
            this._saveAsLayer.Name = "_saveAsLayer";
            this._saveAsLayer.Size = new System.Drawing.Size(188, 22);
            this._saveAsLayer.Text = "Сохранить как";
            this._saveAsLayer.Click += new System.EventHandler(this._saveAsLayer_Click);
            // 
            // _editMenuItem
            // 
            this._editMenuItem.Name = "_editMenuItem";
            this._editMenuItem.Size = new System.Drawing.Size(188, 22);
            this._editMenuItem.Text = "Редактировать стиль";
            this._editMenuItem.Click += new System.EventHandler(this._editMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 638);
            this.Controls.Add(this.groupBoxMap);
            this.Controls.Add(this.groupBoxLayers);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MiniGIS";
            this.Resize += new System.EventHandler(this.EntireView);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.groupBoxLayers.ResumeLayout(false);
            this.groupBoxMap.ResumeLayout(false);
            this.groupBoxMap.PerformLayout();
            this._layerContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Map map;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton panBtn;
        private System.Windows.Forms.ToolStripButton zoomInBtn;
        private System.Windows.Forms.ToolStripButton zoomOutBtn;
        private System.Windows.Forms.ToolStripButton entireViewBtn;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _mapStatus;
        private System.Windows.Forms.ListView listViewLayers;
        private System.Windows.Forms.GroupBox groupBoxLayers;
        private System.Windows.Forms.GroupBox groupBoxMap;
        private System.Windows.Forms.ToolStripButton openLayerBtn;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ToolStripButton _repairGridBtn;
        private System.Windows.Forms.ToolStripStatusLabel _timeStatus;
        private System.Windows.Forms.ContextMenuStrip _layerContextMenu;
        private System.Windows.Forms.ToolStripMenuItem _editMenuItem;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.ToolStripMenuItem _saveAsLayer;
    }
}


﻿namespace MiniGIS.Controls
{
    partial class RepairGridForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepairGridForm));
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.numericUpDownPower = new System.Windows.Forms.NumericUpDown();
            this.lblPower = new System.Windows.Forms.Label();
            this.groupBoxPoints = new System.Windows.Forms.GroupBox();
            this.numericUpDownRadius = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCount = new System.Windows.Forms.NumericUpDown();
            this.radioBtnRadius = new System.Windows.Forms.RadioButton();
            this.radioBtnCount = new System.Windows.Forms.RadioButton();
            this.comboBoxLayers = new System.Windows.Forms.ComboBox();
            this.lblLayer = new System.Windows.Forms.Label();
            this.groupBoxGeometry = new System.Windows.Forms.GroupBox();
            this.txtBoxPointsX = new System.Windows.Forms.TextBox();
            this.txtBoxPointsY = new System.Windows.Forms.TextBox();
            this.numericUpDownStep = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMaxY = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMinY = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMaxX = new System.Windows.Forms.NumericUpDown();
            this.lblPoints = new System.Windows.Forms.Label();
            this.lblStep = new System.Windows.Forms.Label();
            this.numericUpDownMinX = new System.Windows.Forms.NumericUpDown();
            this.lblMax = new System.Windows.Forms.Label();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBoxData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPower)).BeginInit();
            this.groupBoxPoints.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCount)).BeginInit();
            this.groupBoxGeometry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinX)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxData
            // 
            this.groupBoxData.Controls.Add(this.numericUpDownPower);
            this.groupBoxData.Controls.Add(this.lblPower);
            this.groupBoxData.Controls.Add(this.groupBoxPoints);
            this.groupBoxData.Controls.Add(this.comboBoxLayers);
            this.groupBoxData.Controls.Add(this.lblLayer);
            this.groupBoxData.Location = new System.Drawing.Point(9, 10);
            this.groupBoxData.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxData.Size = new System.Drawing.Size(171, 143);
            this.groupBoxData.TabIndex = 1;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = "Данные";
            // 
            // numericUpDownPower
            // 
            this.numericUpDownPower.Location = new System.Drawing.Point(106, 113);
            this.numericUpDownPower.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownPower.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDownPower.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPower.Name = "numericUpDownPower";
            this.numericUpDownPower.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownPower.TabIndex = 4;
            this.numericUpDownPower.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownPower.Validated += new System.EventHandler(this.numericUpDownPower_Validated);
            // 
            // lblPower
            // 
            this.lblPower.AutoSize = true;
            this.lblPower.Location = new System.Drawing.Point(9, 115);
            this.lblPower.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPower.Name = "lblPower";
            this.lblPower.Size = new System.Drawing.Size(94, 13);
            this.lblPower.TabIndex = 3;
            this.lblPower.Text = "Степень влияния";
            // 
            // groupBoxPoints
            // 
            this.groupBoxPoints.Controls.Add(this.numericUpDownRadius);
            this.groupBoxPoints.Controls.Add(this.numericUpDownCount);
            this.groupBoxPoints.Controls.Add(this.radioBtnRadius);
            this.groupBoxPoints.Controls.Add(this.radioBtnCount);
            this.groupBoxPoints.Location = new System.Drawing.Point(7, 41);
            this.groupBoxPoints.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxPoints.Name = "groupBoxPoints";
            this.groupBoxPoints.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxPoints.Size = new System.Drawing.Size(158, 68);
            this.groupBoxPoints.TabIndex = 2;
            this.groupBoxPoints.TabStop = false;
            this.groupBoxPoints.Text = "Поиск ближайших точек";
            // 
            // numericUpDownRadius
            // 
            this.numericUpDownRadius.DecimalPlaces = 1;
            this.numericUpDownRadius.Enabled = false;
            this.numericUpDownRadius.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownRadius.Location = new System.Drawing.Point(99, 42);
            this.numericUpDownRadius.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDownRadius.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRadius.Name = "numericUpDownRadius";
            this.numericUpDownRadius.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownRadius.TabIndex = 12;
            this.numericUpDownRadius.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRadius.Validated += new System.EventHandler(this.numericUpDownRadius_Validated);
            // 
            // numericUpDownCount
            // 
            this.numericUpDownCount.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownCount.Location = new System.Drawing.Point(99, 16);
            this.numericUpDownCount.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCount.Name = "numericUpDownCount";
            this.numericUpDownCount.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownCount.TabIndex = 2;
            this.numericUpDownCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCount.Validated += new System.EventHandler(this.numericUpDownCount_Validated);
            // 
            // radioBtnRadius
            // 
            this.radioBtnRadius.AutoSize = true;
            this.radioBtnRadius.Location = new System.Drawing.Point(4, 43);
            this.radioBtnRadius.Margin = new System.Windows.Forms.Padding(2);
            this.radioBtnRadius.Name = "radioBtnRadius";
            this.radioBtnRadius.Size = new System.Drawing.Size(61, 17);
            this.radioBtnRadius.TabIndex = 1;
            this.radioBtnRadius.Text = "Радиус";
            this.radioBtnRadius.UseVisualStyleBackColor = true;
            this.radioBtnRadius.CheckedChanged += new System.EventHandler(this.radioBtnRadius_CheckedChanged);
            // 
            // radioBtnCount
            // 
            this.radioBtnCount.AutoSize = true;
            this.radioBtnCount.Checked = true;
            this.radioBtnCount.Location = new System.Drawing.Point(4, 17);
            this.radioBtnCount.Margin = new System.Windows.Forms.Padding(2);
            this.radioBtnCount.Name = "radioBtnCount";
            this.radioBtnCount.Size = new System.Drawing.Size(84, 17);
            this.radioBtnCount.TabIndex = 0;
            this.radioBtnCount.TabStop = true;
            this.radioBtnCount.Text = "Количество";
            this.radioBtnCount.UseVisualStyleBackColor = true;
            this.radioBtnCount.CheckedChanged += new System.EventHandler(this.radioBtnCount_CheckedChanged);
            // 
            // comboBoxLayers
            // 
            this.comboBoxLayers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLayers.FormattingEnabled = true;
            this.comboBoxLayers.Location = new System.Drawing.Point(54, 17);
            this.comboBoxLayers.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxLayers.Name = "comboBoxLayers";
            this.comboBoxLayers.Size = new System.Drawing.Size(107, 21);
            this.comboBoxLayers.TabIndex = 1;
            this.comboBoxLayers.SelectedIndexChanged += new System.EventHandler(this.comboBoxLayers_SelectedIndexChanged);
            // 
            // lblLayer
            // 
            this.lblLayer.AutoSize = true;
            this.lblLayer.Location = new System.Drawing.Point(9, 20);
            this.lblLayer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLayer.Name = "lblLayer";
            this.lblLayer.Size = new System.Drawing.Size(32, 13);
            this.lblLayer.TabIndex = 0;
            this.lblLayer.Text = "Слой";
            // 
            // groupBoxGeometry
            // 
            this.groupBoxGeometry.Controls.Add(this.txtBoxPointsX);
            this.groupBoxGeometry.Controls.Add(this.txtBoxPointsY);
            this.groupBoxGeometry.Controls.Add(this.numericUpDownStep);
            this.groupBoxGeometry.Controls.Add(this.numericUpDownMaxY);
            this.groupBoxGeometry.Controls.Add(this.numericUpDownMinY);
            this.groupBoxGeometry.Controls.Add(this.numericUpDownMaxX);
            this.groupBoxGeometry.Controls.Add(this.lblPoints);
            this.groupBoxGeometry.Controls.Add(this.lblStep);
            this.groupBoxGeometry.Controls.Add(this.numericUpDownMinX);
            this.groupBoxGeometry.Controls.Add(this.lblMax);
            this.groupBoxGeometry.Controls.Add(this.lblMin);
            this.groupBoxGeometry.Controls.Add(this.lblY);
            this.groupBoxGeometry.Controls.Add(this.lblX);
            this.groupBoxGeometry.Location = new System.Drawing.Point(184, 10);
            this.groupBoxGeometry.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxGeometry.Name = "groupBoxGeometry";
            this.groupBoxGeometry.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxGeometry.Size = new System.Drawing.Size(226, 143);
            this.groupBoxGeometry.TabIndex = 2;
            this.groupBoxGeometry.TabStop = false;
            this.groupBoxGeometry.Text = "Геометрия";
            // 
            // txtBoxPointsX
            // 
            this.txtBoxPointsX.Location = new System.Drawing.Point(44, 112);
            this.txtBoxPointsX.Name = "txtBoxPointsX";
            this.txtBoxPointsX.ReadOnly = true;
            this.txtBoxPointsX.Size = new System.Drawing.Size(85, 20);
            this.txtBoxPointsX.TabIndex = 5;
            // 
            // txtBoxPointsY
            // 
            this.txtBoxPointsY.Location = new System.Drawing.Point(135, 112);
            this.txtBoxPointsY.Name = "txtBoxPointsY";
            this.txtBoxPointsY.ReadOnly = true;
            this.txtBoxPointsY.Size = new System.Drawing.Size(86, 20);
            this.txtBoxPointsY.TabIndex = 6;
            // 
            // numericUpDownStep
            // 
            this.numericUpDownStep.DecimalPlaces = 1;
            this.numericUpDownStep.Location = new System.Drawing.Point(44, 86);
            this.numericUpDownStep.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDownStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownStep.Name = "numericUpDownStep";
            this.numericUpDownStep.Size = new System.Drawing.Size(177, 20);
            this.numericUpDownStep.TabIndex = 9;
            this.numericUpDownStep.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownStep.Validated += new System.EventHandler(this.numericUpDownStep_Validated);
            // 
            // numericUpDownMaxY
            // 
            this.numericUpDownMaxY.DecimalPlaces = 1;
            this.numericUpDownMaxY.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMaxY.Location = new System.Drawing.Point(135, 60);
            this.numericUpDownMaxY.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDownMaxY.Minimum = new decimal(new int[] {
            10000000,
            0,
            0,
            -2147483648});
            this.numericUpDownMaxY.Name = "numericUpDownMaxY";
            this.numericUpDownMaxY.Size = new System.Drawing.Size(86, 20);
            this.numericUpDownMaxY.TabIndex = 7;
            this.numericUpDownMaxY.Validating += new System.ComponentModel.CancelEventHandler(this.ValidatingY);
            this.numericUpDownMaxY.Validated += new System.EventHandler(this.numericUpDownMaxY_Validated);
            // 
            // numericUpDownMinY
            // 
            this.numericUpDownMinY.DecimalPlaces = 1;
            this.numericUpDownMinY.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMinY.Location = new System.Drawing.Point(44, 60);
            this.numericUpDownMinY.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDownMinY.Minimum = new decimal(new int[] {
            10000000,
            0,
            0,
            -2147483648});
            this.numericUpDownMinY.Name = "numericUpDownMinY";
            this.numericUpDownMinY.Size = new System.Drawing.Size(85, 20);
            this.numericUpDownMinY.TabIndex = 8;
            this.numericUpDownMinY.Validating += new System.ComponentModel.CancelEventHandler(this.ValidatingY);
            this.numericUpDownMinY.Validated += new System.EventHandler(this.numericUpDownMinY_Validated);
            // 
            // numericUpDownMaxX
            // 
            this.numericUpDownMaxX.DecimalPlaces = 1;
            this.numericUpDownMaxX.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMaxX.Location = new System.Drawing.Point(135, 34);
            this.numericUpDownMaxX.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDownMaxX.Minimum = new decimal(new int[] {
            10000000,
            0,
            0,
            -2147483648});
            this.numericUpDownMaxX.Name = "numericUpDownMaxX";
            this.numericUpDownMaxX.Size = new System.Drawing.Size(86, 20);
            this.numericUpDownMaxX.TabIndex = 6;
            this.numericUpDownMaxX.Validating += new System.ComponentModel.CancelEventHandler(this.ValidatingX);
            this.numericUpDownMaxX.Validated += new System.EventHandler(this.numericUpDownMaxX_Validated);
            // 
            // lblPoints
            // 
            this.lblPoints.AutoSize = true;
            this.lblPoints.Location = new System.Drawing.Point(3, 115);
            this.lblPoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPoints.Name = "lblPoints";
            this.lblPoints.Size = new System.Drawing.Size(37, 13);
            this.lblPoints.TabIndex = 5;
            this.lblPoints.Text = "Точек";
            // 
            // lblStep
            // 
            this.lblStep.AutoSize = true;
            this.lblStep.Location = new System.Drawing.Point(16, 88);
            this.lblStep.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStep.Name = "lblStep";
            this.lblStep.Size = new System.Drawing.Size(27, 13);
            this.lblStep.TabIndex = 4;
            this.lblStep.Text = "Шаг";
            // 
            // numericUpDownMinX
            // 
            this.numericUpDownMinX.DecimalPlaces = 1;
            this.numericUpDownMinX.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMinX.Location = new System.Drawing.Point(44, 34);
            this.numericUpDownMinX.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDownMinX.Minimum = new decimal(new int[] {
            10000000,
            0,
            0,
            -2147483648});
            this.numericUpDownMinX.Name = "numericUpDownMinX";
            this.numericUpDownMinX.Size = new System.Drawing.Size(85, 20);
            this.numericUpDownMinX.TabIndex = 5;
            this.numericUpDownMinX.Validating += new System.ComponentModel.CancelEventHandler(this.ValidatingX);
            this.numericUpDownMinX.Validated += new System.EventHandler(this.numericUpDownMinX_Validated);
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.Location = new System.Drawing.Point(147, 17);
            this.lblMax.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(61, 13);
            this.lblMax.TabIndex = 3;
            this.lblMax.Text = "Максимум";
            // 
            // lblMin
            // 
            this.lblMin.AutoSize = true;
            this.lblMin.Location = new System.Drawing.Point(59, 17);
            this.lblMin.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(55, 13);
            this.lblMin.TabIndex = 2;
            this.lblMin.Text = "Минимум";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(25, 62);
            this.lblY.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(14, 13);
            this.lblY.TabIndex = 1;
            this.lblY.Text = "Y";
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(25, 36);
            this.lblX.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(14, 13);
            this.lblX.TabIndex = 0;
            this.lblX.Text = "X";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(354, 161);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(56, 20);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(294, 161);
            this.btnOk.Margin = new System.Windows.Forms.Padding(2);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(56, 20);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "ОК";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // RepairGridForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 189);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBoxGeometry);
            this.Controls.Add(this.groupBoxData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RepairGridForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Восстановление регулярной сети";
            this.groupBoxData.ResumeLayout(false);
            this.groupBoxData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPower)).EndInit();
            this.groupBoxPoints.ResumeLayout(false);
            this.groupBoxPoints.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCount)).EndInit();
            this.groupBoxGeometry.ResumeLayout(false);
            this.groupBoxGeometry.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBoxData;
        private System.Windows.Forms.GroupBox groupBoxGeometry;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblLayer;
        private System.Windows.Forms.ComboBox comboBoxLayers;
        private System.Windows.Forms.GroupBox groupBoxPoints;
        private System.Windows.Forms.NumericUpDown numericUpDownCount;
        private System.Windows.Forms.RadioButton radioBtnRadius;
        private System.Windows.Forms.RadioButton radioBtnCount;
        private System.Windows.Forms.Label lblPower;
        private System.Windows.Forms.NumericUpDown numericUpDownPower;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Label lblPoints;
        private System.Windows.Forms.Label lblStep;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.NumericUpDown numericUpDownStep;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxY;
        private System.Windows.Forms.NumericUpDown numericUpDownMinY;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxX;
        private System.Windows.Forms.NumericUpDown numericUpDownMinX;
        private System.Windows.Forms.NumericUpDown numericUpDownRadius;
        private System.Windows.Forms.TextBox txtBoxPointsX;
        private System.Windows.Forms.TextBox txtBoxPointsY;
    }
}
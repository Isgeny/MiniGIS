﻿namespace MiniGIS.Controls
{
    partial class SymbolSizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SymbolSizeForm));
            this._fixedSizeRadioBtn = new System.Windows.Forms.RadioButton();
            this._proportionalSizeRadioBtn = new System.Windows.Forms.RadioButton();
            this._btnOk = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._fixedSizeUpDown = new System.Windows.Forms.NumericUpDown();
            this._minimumSizeUpDown = new System.Windows.Forms.NumericUpDown();
            this._maximumSizeUpDown = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this._fixedSizeUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._minimumSizeUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._maximumSizeUpDown)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _fixedSizeRadioBtn
            // 
            this._fixedSizeRadioBtn.AutoSize = true;
            this._fixedSizeRadioBtn.Checked = true;
            this._fixedSizeRadioBtn.Location = new System.Drawing.Point(21, 12);
            this._fixedSizeRadioBtn.Name = "_fixedSizeRadioBtn";
            this._fixedSizeRadioBtn.Size = new System.Drawing.Size(151, 17);
            this._fixedSizeRadioBtn.TabIndex = 0;
            this._fixedSizeRadioBtn.TabStop = true;
            this._fixedSizeRadioBtn.Text = "Фиксированный размер";
            this._fixedSizeRadioBtn.UseVisualStyleBackColor = true;
            this._fixedSizeRadioBtn.Click += new System.EventHandler(this._fixedSizeRadioBtn_Click);
            // 
            // _proportionalSizeRadioBtn
            // 
            this._proportionalSizeRadioBtn.AutoSize = true;
            this._proportionalSizeRadioBtn.Location = new System.Drawing.Point(9, -2);
            this._proportionalSizeRadioBtn.Name = "_proportionalSizeRadioBtn";
            this._proportionalSizeRadioBtn.Size = new System.Drawing.Size(170, 17);
            this._proportionalSizeRadioBtn.TabIndex = 1;
            this._proportionalSizeRadioBtn.Text = "Метод размерных символов";
            this._proportionalSizeRadioBtn.UseVisualStyleBackColor = true;
            this._proportionalSizeRadioBtn.Click += new System.EventHandler(this._proportionalSizeRadioBtn_Click);
            // 
            // _btnOk
            // 
            this._btnOk.Location = new System.Drawing.Point(99, 121);
            this._btnOk.Name = "_btnOk";
            this._btnOk.Size = new System.Drawing.Size(75, 23);
            this._btnOk.TabIndex = 2;
            this._btnOk.Text = "ОК";
            this._btnOk.UseVisualStyleBackColor = true;
            this._btnOk.Click += new System.EventHandler(this._btnOk_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.Location = new System.Drawing.Point(180, 121);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(75, 23);
            this._btnCancel.TabIndex = 3;
            this._btnCancel.Text = "Отмена";
            this._btnCancel.UseVisualStyleBackColor = true;
            this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Минимальный размер";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Максимальный размер";
            // 
            // _fixedSizeUpDown
            // 
            this._fixedSizeUpDown.Location = new System.Drawing.Point(178, 12);
            this._fixedSizeUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._fixedSizeUpDown.Name = "_fixedSizeUpDown";
            this._fixedSizeUpDown.Size = new System.Drawing.Size(68, 20);
            this._fixedSizeUpDown.TabIndex = 9;
            this._fixedSizeUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // _minimumSizeUpDown
            // 
            this._minimumSizeUpDown.Location = new System.Drawing.Point(166, 48);
            this._minimumSizeUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._minimumSizeUpDown.Name = "_minimumSizeUpDown";
            this._minimumSizeUpDown.Size = new System.Drawing.Size(68, 20);
            this._minimumSizeUpDown.TabIndex = 10;
            this._minimumSizeUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this._minimumSizeUpDown.Validating += new System.ComponentModel.CancelEventHandler(this.ValidatingSize);
            // 
            // _maximumSizeUpDown
            // 
            this._maximumSizeUpDown.Location = new System.Drawing.Point(166, 22);
            this._maximumSizeUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._maximumSizeUpDown.Name = "_maximumSizeUpDown";
            this._maximumSizeUpDown.Size = new System.Drawing.Size(68, 20);
            this._maximumSizeUpDown.TabIndex = 12;
            this._maximumSizeUpDown.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this._maximumSizeUpDown.Validating += new System.ComponentModel.CancelEventHandler(this.ValidatingSize);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._proportionalSizeRadioBtn);
            this.groupBox1.Controls.Add(this._maximumSizeUpDown);
            this.groupBox1.Controls.Add(this._minimumSizeUpDown);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 77);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // SymbolSizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 153);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._fixedSizeUpDown);
            this.Controls.Add(this._fixedSizeRadioBtn);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this._btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SymbolSizeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Задание размера символов";
            ((System.ComponentModel.ISupportInitialize)(this._fixedSizeUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._minimumSizeUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._maximumSizeUpDown)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton _fixedSizeRadioBtn;
        private System.Windows.Forms.RadioButton _proportionalSizeRadioBtn;
        private System.Windows.Forms.Button _btnOk;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown _fixedSizeUpDown;
        private System.Windows.Forms.NumericUpDown _minimumSizeUpDown;
        private System.Windows.Forms.NumericUpDown _maximumSizeUpDown;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
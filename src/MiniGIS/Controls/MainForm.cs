namespace MiniGIS.Controls
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows.Forms;

    using MiniGIS.Grid;
    using MiniGIS.Interfaces;
    using MiniGIS.Repositories;
    using MiniGIS.Vector;

    using Point = System.Drawing.Point;

    public partial class MainForm : Form
    {
        private readonly ILayerRepositoriesFactory _layerRepositoriesFactory;

        private ToolStripButton _currentToolBtn;

        public MainForm()
        {
            InitializeComponent();

            _layerRepositoriesFactory = new LayerRepositoriesFactory();
            _currentToolBtn = panBtn;
            _currentToolBtn.Checked = true;
        }

        private void _editMenuItem_Click(object sender, EventArgs e)
        {
            var layerListViewItemIndex = listViewLayers.FocusedItem.Index;
            var layer = map.Layers[layerListViewItemIndex];

            if (layer is GridLayer grid)
            {
                var gridStyleForm = new GridStyleForm();
                if (gridStyleForm.ShowStyleDialog(grid.Colors, out var colors) == DialogResult.OK)
                    grid.Colors = colors;
            }
            else if (layer is VectorLayer vectorLayer)
            {
                var symbolSizeForm = new SymbolSizeForm();
                symbolSizeForm.ShowSymbolSizeDialog(vectorLayer);
            }

            map.Refresh();
        }

        private void _saveAsLayer_Click(object sender, EventArgs e)
        {
            var layerListViewItemIndex = listViewLayers.FocusedItem.Index;
            var layer = map.Layers[layerListViewItemIndex];

            if (!(layer is GridLayer grid))
                return;

            var saveFileDialog = new SaveFileDialog
            {
                Filter = @"Json files|*.json",
                FileName = layer.Name
            };

            if (saveFileDialog.ShowDialog() != DialogResult.OK)
                return;

            var repository = _layerRepositoriesFactory.GetLayersRepository(saveFileDialog.FileName);
            if (repository is IGridLayersRepository gridRepository)
                gridRepository.CreateLayer(grid);
        }

        private void AddLayer(BaseLayer layer)
        {
            map.AddLayer(layer);
            var listViewItem = new ListViewItem(layer.Name) { Checked = true };
            listViewLayers.Items.Add(listViewItem);
        }

        private void EntireView(object sender, EventArgs e)
        {
            map.EntireView();
        }

        private void listViewLayers_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void listViewLayers_DragOver(object sender, DragEventArgs e)
        {
            if (listViewLayers.SelectedItems.Count == 0)
                return;

            var point = listViewLayers.PointToClient(new Point(e.X, e.Y));
            var dragItem = listViewLayers.GetItemAt(point.X, point.Y);
            if (dragItem == null)
                return;

            var itemDragIndex = dragItem.Index;
            var selectedItem = listViewLayers.SelectedItems[0];
            var selectedItemIndex = selectedItem.Index;
            if (itemDragIndex != selectedItemIndex)
            {
                listViewLayers.Items.Remove(selectedItem);
                var selectedLayer = map.Layers[selectedItemIndex];
                map.RemoveLayer(selectedItemIndex);
                map.InsertLayer(itemDragIndex, selectedLayer);
                listViewLayers.Items.Insert(itemDragIndex, selectedItem);
            }

            map.Refresh();
        }

        private void listViewLayers_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            var itemIndex = e.Item.Index;
            map.Layers[itemIndex].Visible = e.Item.Checked;
            map.Refresh();
        }

        private void listViewLayers_ItemDrag(object sender, ItemDragEventArgs e)
        {
            listViewLayers.DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private void listViewLayers_KeyUp(object sender, KeyEventArgs e)
        {
            if (listViewLayers.SelectedItems.Count == 0 || e.KeyCode != Keys.Delete)
                return;

            var itemIndex = listViewLayers.SelectedItems[0].Index;
            map.RemoveLayer(itemIndex);
            listViewLayers.Items.RemoveAt(itemIndex);
            map.Refresh();
        }

        private void listViewLayers_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && listViewLayers.FocusedItem.Bounds.Contains(e.Location))
                _layerContextMenu.Show(Cursor.Position);
        }

        private void listViewLayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewLayers.SelectedItems.Count == 0)
                return;

            var selectedIndex = listViewLayers.SelectedItems[0].Index;
            map.SelectedLayer = map.Layers[selectedIndex];
        }

        private void mapControl_MouseMove(object sender, MouseEventArgs e)
        {
            var mapCenter = "Center: " + map.MapCenter.X.ToString("0.0") + " : " + map.MapCenter.Y.ToString("0.0");
            var mapScale = "Scale: " + map.MapScale.ToString("0.0");
            var screenCoord = "Screen: " + e.X.ToString("0.0") + " : " + e.Y.ToString("0.0");
            var mapCoord = "Map: " +
                           map.ScreenToMap(e.Location).X.ToString("0.0") + " : " +
                           map.ScreenToMap(e.Location).Y.ToString("0.0");
            _mapStatus.Text = $@"{mapCenter} | {mapScale} | {mapCoord} | {screenCoord}";
        }

        private void OnGridLayerCalculationProgress(object sender, int percent)
        {
            _progressBar.Value = percent;
            if (percent > 0)
                _progressBar.Value = percent - 1;
        }

        private void OpenLayer()
        {
            var fileDialog = new OpenFileDialog
            {
                Filter =
                    @"All files|*.mif;*.txt;*.csv;*.tsv;*.json|Mif files|*.mif|Dsv files|*.txt;*.csv;*.tsv|Json files|*.json",
                Multiselect = true
            };

            if (fileDialog.ShowDialog() != DialogResult.OK)
                return;

            foreach (var filePath in fileDialog.FileNames)
            {
                var repository = _layerRepositoriesFactory.GetLayersRepository(filePath);
                var layer = repository.GetLayer();
                AddLayer(layer);
            }

            map.EntireView();
        }

        private void OpenLayerHandler(object sender, EventArgs e)
        {
            OpenLayer();
        }

        private async void RepairGridHandler(object sender, EventArgs e)
        {
            var pointLayers = map.GetPointLayers();
            if (!pointLayers.Any())
            {
                OpenLayer();
                return;
            }

            var selectedLayer = map.SelectedLayer as VectorLayer ?? pointLayers.First();

            var repairGridForm = new RepairGridForm();
            var dialogResult = repairGridForm.ShowForCalc(pointLayers, selectedLayer,
                out var repairArea, out var geometry);

            if (dialogResult != DialogResult.OK)
                return;

            var grid = new GridLayer(geometry, $"{repairArea.VectorLayer.Name} grid");

            var progress = new Progress<int>();
            progress.ProgressChanged += OnGridLayerCalculationProgress;

            var sw = new Stopwatch();
            sw.Start();
            _timeStatus.Text = null;

            await grid.CalculateAsync(repairArea, progress);

            sw.Stop();
            _progressBar.Value = 0;
            _timeStatus.Text = $@" Time: {sw.ElapsedMilliseconds}";

            AddLayer(grid);
        }

        private void ToolStripButtonClickHandler(object sender, EventArgs e)
        {
            var toolStripButton = (ToolStripButton) sender;
            if (!Enum.TryParse(toolStripButton.Text, out Tools currentTool))
                return;

            map.CurrentTools = currentTool;
            _currentToolBtn.Checked = false;
            _currentToolBtn = toolStripButton;
            _currentToolBtn.Checked = true;
        }
    }
}
﻿namespace MiniGIS.Controls
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    using MiniGIS.Vector;

    public partial class SymbolSizeForm : Form
    {
        private PointsSizeMethod _pointsSizeMethod;

        public SymbolSizeForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowSymbolSizeDialog(VectorLayer vectorLayer)
        {
            var layerInterpolator = vectorLayer.PointsInterpolator;

            _fixedSizeUpDown.Value = Convert.ToDecimal(vectorLayer.FixedSymbolSize);
            _minimumSizeUpDown.Value = Convert.ToDecimal(layerInterpolator.Y1);
            _maximumSizeUpDown.Value = Convert.ToDecimal(layerInterpolator.Y2);

            if (vectorLayer.PointsSizeMethod == PointsSizeMethod.FixedSize)
                SetFixedSizeMethod();
            else
                SetProportionalMethod();

            var dialogResult = ShowDialog();
            if (dialogResult != DialogResult.OK)
                return dialogResult;

            vectorLayer.PointsSizeMethod = _pointsSizeMethod;
            if (_pointsSizeMethod == PointsSizeMethod.FixedSize)
            {
                vectorLayer.FixedSymbolSize = Convert.ToDouble(_fixedSizeUpDown.Value);
            }
            else
            {
                layerInterpolator.Y1 = Convert.ToDouble(_minimumSizeUpDown.Value);
                layerInterpolator.Y2 = Convert.ToDouble(_maximumSizeUpDown.Value);
            }

            return DialogResult.OK;
        }

        private void _btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void _btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void _fixedSizeRadioBtn_Click(object sender, EventArgs e)
        {
            SetFixedSizeMethod();
        }

        private void _proportionalSizeRadioBtn_Click(object sender, EventArgs e)
        {
            SetProportionalMethod();
        }

        private void SetFixedSizeMethod()
        {
            _pointsSizeMethod = PointsSizeMethod.FixedSize;
            _fixedSizeRadioBtn.Checked = true;
            _proportionalSizeRadioBtn.Checked = false;
            _fixedSizeUpDown.Enabled = true;
            _minimumSizeUpDown.Enabled = false;
            _maximumSizeUpDown.Enabled = false;
        }

        private void SetProportionalMethod()
        {
            _pointsSizeMethod = PointsSizeMethod.ProportionalSize;
            _fixedSizeRadioBtn.Checked = false;
            _proportionalSizeRadioBtn.Checked = true;
            _fixedSizeUpDown.Enabled = false;
            _minimumSizeUpDown.Enabled = true;
            _maximumSizeUpDown.Enabled = true;
        }

        private void ValidatingSize(object sender, CancelEventArgs e)
        {
            if (_minimumSizeUpDown.Value > _maximumSizeUpDown.Value)
                e.Cancel = true;
        }
    }
}
﻿namespace MiniGIS.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using MiniGIS.Vector;

    using Point = System.Drawing.Point;

    public partial class Map : UserControl
    {
        private const int SHAKE = 5;

        private bool _isMouseDown;

        private Point _mouseDownPosition;

        public Map()
        {
            InitializeComponent();
            
            _isMouseDown = false;

            CurrentTools = Tools.Pan;
            MapScale = 1.0;
            MapCenter = new GeoPoint();
            Layers = new List<BaseLayer>();
        }

        public Tools CurrentTools { get; set; }

        public List<BaseLayer> Layers { get; }

        public GeoPoint MapCenter { get; set; }

        public double MapScale { get; set; }

        public BaseLayer SelectedLayer { get; set; }

        public void AddLayer(BaseLayer layer)
        {
            if (SelectedLayer == null)
                SelectedLayer = layer;

            layer.Map = this;
            Layers.Add(layer);
        }

        public void EntireView()
        {
            var bounds = GetBounds();
            var dx = Math.Abs(bounds.XMax - bounds.XMin);
            var dy = Math.Abs(bounds.YMax - bounds.YMin);
            var newCenter = new GeoPoint((bounds.XMin + bounds.XMax) / 2.0, (bounds.YMin + bounds.YMax) / 2.0);
            MapCenter = newCenter;

            if (dx == 0.0 || dy == 0.0)
            {
                MapScale = 1.0;
                return;
            }

            if (Width / dx < Height / dy)
                MapScale = Width / dx;
            else
                MapScale = Height / dy;

            Refresh();
        }

        public List<VectorLayer> GetPointLayers()
        {
            return Layers.OfType<VectorLayer>()
                .Where(l => l.MapObjects.All(o => o is Vector.Point))
                .ToList();
        }

        public void InsertLayer(int index, BaseLayer layer)
        {
            Layers.Insert(index, layer);
        }

        public Point MapToScreen(GeoPoint mapPoint)
        {
            return new Point
            {
                X = (int) ((mapPoint.X - MapCenter.X) * MapScale + Width / 2.0 + 0.5),
                Y = (int) (-(mapPoint.Y - MapCenter.Y) * MapScale + Height / 2.0 + 0.5)
            };
        }

        public Point MapToScreen(double x, double y)
        {
            return MapToScreen(new GeoPoint(x, y));
        }

        public void RemoveLayer(int index)
        {
            var layer = Layers[index];
            if (layer == SelectedLayer)
                SelectedLayer = Layers.FirstOrDefault();

            Layers.RemoveAt(index);
        }

        public GeoPoint ScreenToMap(Point screenPoint)
        {
            return new GeoPoint
            {
                X = (screenPoint.X - Width / 2) / MapScale + MapCenter.X,
                Y = -(screenPoint.Y - Height / 2) / MapScale + MapCenter.Y
            };
        }

        public GeoPoint ScreenToMap(int x, int y)
        {
            return ScreenToMap(new Point(x, y));
        }

        private GeoRect GetBounds()
        {
            var result = new GeoRect(0.0, 0.0, 0.0, 0.0);
            return Layers.Where(layer => layer.Visible)
                .Aggregate(result, (current, layer) => GeoRect.Union(current, layer.Bounds));
        }

        private void Map_MouseDown(object sender, MouseEventArgs e)
        {
            _isMouseDown = true;
            _mouseDownPosition = e.Location;
            if (CurrentTools == Tools.Pan)
                Cursor = Cursors.Hand;
        }

        private void Map_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isMouseDown)
                return;

            switch (CurrentTools)
            {
                case Tools.Pan:
                    MapCenter.X += (_mouseDownPosition.X - e.X) / MapScale;
                    MapCenter.Y -= (_mouseDownPosition.Y - e.Y) / MapScale;
                    _mouseDownPosition = e.Location;
                    break;

                case Tools.ZoomIn:
                    if (Math.Abs(_mouseDownPosition.X - e.X) > SHAKE || Math.Abs(_mouseDownPosition.Y - e.Y) > SHAKE)
                    {
                        var frame = new PolyLine();
                        frame.AddNode(ScreenToMap(_mouseDownPosition.X, _mouseDownPosition.Y));
                        frame.AddNode(ScreenToMap(e.X, _mouseDownPosition.Y));
                        frame.AddNode(ScreenToMap(e.X, e.Y));
                        frame.AddNode(ScreenToMap(_mouseDownPosition.X, e.Y));
                        frame.AddNode(ScreenToMap(_mouseDownPosition.X, _mouseDownPosition.Y));
                    }
                    break;
            }

            Refresh();
        }

        private void Map_MouseUp(object sender, MouseEventArgs e)
        {
            double newScale;
            switch (CurrentTools)
            {
                case Tools.Pan:
                    Cursor = Cursors.Default;
                    break;

                case Tools.ZoomIn:
                    double dx = Math.Abs(_mouseDownPosition.X - e.X);
                    double dy = Math.Abs(_mouseDownPosition.Y - e.Y);

                    // Приближение c помощью рамки
                    if (dx > SHAKE || dy > SHAKE)
                    {
                        var newCenter = ScreenToMap((e.X + _mouseDownPosition.X) / 2,
                            (e.Y + _mouseDownPosition.Y) / 2);
                        MapCenter = newCenter;
                        if (Width / dx > Height / dy)
                            newScale = MapScale * Width / dx;
                        else
                            newScale = MapScale * Height / dy;
                        MapScale = newScale;
                    }
                    // Точечное приближение
                    else
                    {
                        MapCenter = ScreenToMap(_mouseDownPosition);
                        newScale = MapScale * 2.0;
                        MapScale = newScale;
                    }
                    break;

                case Tools.ZoomOut:
                    MapCenter = ScreenToMap(_mouseDownPosition);
                    newScale = MapScale / 2.0;
                    MapScale = newScale;
                    break;
            }

            _isMouseDown = false;
            Refresh();
        }

        private void Map_Paint(object sender, PaintEventArgs e)
        {
            for (var i = Layers.Count - 1; i >= 0; i--)
                Layers[i].Draw(e);
        }
    }
}
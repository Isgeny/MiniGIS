﻿namespace MiniGIS.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using MiniGIS.Grid;

    public partial class GridStyleForm : Form
    {
        public GridStyleForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowStyleDialog(List<GridColor> inputColors, out List<GridColor> outputColors)
        {
            inputColors.ForEach(c => _colorsDataSource.Add(c));
            outputColors = null;

            var dialogResult = ShowDialog();
            if (dialogResult != DialogResult.OK)
                return dialogResult;

            outputColors = _colorsDataSource.OfType<GridColor>().ToList();

            return dialogResult;
        }

        private void _btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void _btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void _colorsDataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (_colorsDataGrid?.CurrentRow?.Cells["Color"]?.Selected != true)
                return;

            var colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;

            var color = colorDialog.Color;
            var colorCell = _colorsDataGrid.CurrentRow.Cells["Color"];

            colorCell.Value = color;
            colorCell.Style.BackColor = color;
            colorCell.Style.SelectionBackColor = color;
        }

        private void _colorsDataGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            for (var i = 0; i < _colorsDataGrid.Rows.Count; i++)
            {
                var colorCell = _colorsDataGrid.Rows[i].Cells["Color"];
                if (colorCell.Value == null)
                    continue;

                var color = ((GridColor) _colorsDataSource.List[i]).Color;
                colorCell.Style.BackColor = color;
                colorCell.Style.SelectionBackColor = color;
            }
        }
    }
}
﻿namespace MiniGIS.Repositories
{
    using System.IO;

    using MiniGIS.Grid;
    using MiniGIS.Interfaces;

    using Newtonsoft.Json;

    public class JsonLayerRepository : IGridLayersRepository
    {
        private readonly string _filePath;

        private readonly JsonSerializer _serializer;

        public JsonLayerRepository(string filePath)
        {
            _filePath = filePath;
            _serializer = new JsonSerializer();
        }

        public void CreateLayer(GridLayer grid)
        {
            using (var file = new StreamWriter(_filePath))
            {
                _serializer.Serialize(file, grid);
            }
        }

        public GridLayer GetLayer()
        {
            if (!File.Exists(_filePath))
                return null;

            using (var file = new StreamReader(_filePath))
            {
                var reader = new JsonTextReader(file);
                return _serializer.Deserialize<GridLayer>(reader);
            }
        }
    }
}
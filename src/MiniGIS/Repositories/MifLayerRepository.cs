﻿namespace MiniGIS.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.IO;

    using MiniGIS.Interfaces;
    using MiniGIS.Styles;
    using MiniGIS.Vector;

    using Point = MiniGIS.Vector.Point;

    public class MifLayerRepository : IVectorLayersRepository
    {
        private readonly string _filePath;

        public MifLayerRepository(string filePath)
        {
            _filePath = filePath;
            Version = 300;
            Charset = "";
            Delimiter = '\t';
            Unique = new List<int>();
            Index = new List<int>();
            Transform = new List<int>(4);
            ColumnsN = 0;
        }

        public string Charset { get; set; }

        public List<Column> Columns { get; set; }

        public int ColumnsN { get; set; }

        public char Delimiter { get; set; }

        public List<int> Index { get; set; }

        public List<int> Transform { get; set; }

        public List<int> Unique { get; set; }

        public int Version { get; set; }

        public VectorLayer GetLayer()
        {
            var vectorLayer = new VectorLayer(Path.GetFileNameWithoutExtension(_filePath));

            using (var sr = new StreamReader(_filePath))
            {
                // Версия
                Version = Convert.ToInt32(sr.ReadLine().Split(' ')[1]);

                // Кодировка
                var charset = sr.ReadLine().Split(' ')[1];
                Charset = charset.Substring(1, charset.Length - 2);

                var line = sr.ReadLine().Split(' ');

                // Разделитель
                if (line[0] == "Delimiter")
                {
                    Delimiter = line[1].Substring(1, line[1].Length - 2)[0];
                    line = sr.ReadLine().Split(' ');
                }

                // Уникальная колонка
                if (line[0] == "Unique")
                {
                    var uniqueStr = line[1].Split(',');
                    for (var i = 0; i < uniqueStr.Length; ++i)
                        Unique[i] = Convert.ToInt32(uniqueStr[i]);
                    line = sr.ReadLine().Split(' ');
                }

                // Индекс
                if (line[0] == "Index")
                {
                    var indexStr = line[1].Split(',');
                    for (var i = 0; i < indexStr.Length; ++i)
                        Index[i] = Convert.ToInt32(indexStr[i]);
                    line = sr.ReadLine().Split(' ');
                }

                // Преобразование
                if (line[0] == "Transform")
                {
                    var transformStr = line[1].Split(',');
                    for (var i = 0; i < transformStr.Length; ++i)
                        Transform[i] = Convert.ToInt32(transformStr[i]);
                    line = sr.ReadLine().Split(' ');
                }

                // Колонки
                ColumnsN = Convert.ToInt32(line[1]);
                Columns = new List<Column>(ColumnsN);
                for (var i = 0; i < ColumnsN; ++i)
                {
                    line = sr.ReadLine().Split(' ');
                    var column = new Column(line[0], line[1]);
                }

                // Слово Data
                sr.ReadLine();

                // Данные
                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine().Split(' ');
                    if (line[0] == "Point")
                    {
                        // Координаты точки
                        var x = double.Parse(line[1], CultureInfo.InvariantCulture);
                        var y = double.Parse(line[2], CultureInfo.InvariantCulture);

                        // Symbol
                        var symbolByte = Convert.ToByte(line[4].Substring(1, line[4].Length - 2));
                        var color = IntToColor(Convert.ToInt32(line[5].Substring(0, line[5].Length - 1)));
                        var symbolSize = Convert.ToInt32(line[6].Substring(0, line[6].Length - 1));
                        var fontFamily = line[7].Substring(1, line[7].Length - 3);

                        // Добавление точки в основной список
                        var point = new Point(new GeoPoint(x, y),
                            new PointStyle(fontFamily, symbolByte, color, symbolSize));
                        vectorLayer.AddMapObject(point);
                    }
                    else if (line[0] == "Line")
                    {
                        // Координаты начала и конца линии
                        var x1 = double.Parse(line[1], CultureInfo.InvariantCulture);
                        var y1 = double.Parse(line[2], CultureInfo.InvariantCulture);
                        var x2 = double.Parse(line[3], CultureInfo.InvariantCulture);
                        var y2 = double.Parse(line[4], CultureInfo.InvariantCulture);

                        // Pen
                        var width = float.Parse(line[6].Substring(1, line[6].Length - 1));
                        var color = IntToColor(Convert.ToInt32(line[8].Substring(0, line[8].Length - 1)));

                        // Добавление линии в основной список
                        var mapLine = new Line(new GeoPoint(x1, y1), new GeoPoint(x2, y2), new LineStyle(color, width));
                        vectorLayer.AddMapObject(mapLine);
                    }
                    else if (line[0] == "Pline")
                    {
                        // Количество полилиний
                        var n = Convert.ToInt32(sr.ReadLine().Split(' ')[0]);

                        // Вершины полилинии
                        var pointsList = new List<GeoPoint>();
                        for (var i = 0; i < n; ++i)
                        {
                            line = sr.ReadLine().Split(' ');
                            var x = double.Parse(line[0], CultureInfo.InvariantCulture);
                            var y = double.Parse(line[1], CultureInfo.InvariantCulture);
                            var point = new GeoPoint(x, y);
                            pointsList.Add(point);
                        }

                        // Pen
                        line = sr.ReadLine().Split(' ');
                        var width = float.Parse(line[1].Substring(1, line[0].Length - 2));
                        var color = IntToColor(Convert.ToInt32(line[3].Substring(0, line[3].Length - 1)));

                        // Добавление полилинии в основной список
                        var polyline = new PolyLine(pointsList, new LineStyle(color, width));
                        vectorLayer.AddMapObject(polyline);
                    }
                    else if (line[0] == "Region")
                    {
                        // Количество регионов
                        var regionsNumb = Convert.ToInt32(line[1]);

                        // Вершины полигонов
                        var polygonsList = new List<Polygon>();
                        for (var i = 0; i < regionsNumb; ++i)
                        {
                            line = sr.ReadLine().Split(' ');
                            var pointsNumb = Convert.ToInt32(line[0]);
                            var pointList = new List<GeoPoint>();
                            for (var j = 0; j < pointsNumb; ++j)
                            {
                                line = sr.ReadLine().Split(' ');
                                var x = double.Parse(line[0], CultureInfo.InvariantCulture);
                                var y = double.Parse(line[1], CultureInfo.InvariantCulture);
                                var point = new GeoPoint(x, y);
                                pointList.Add(point);
                            }

                            // Добавление полигонов во временный список
                            var polygon = new Polygon(pointList, new LineStyle(), new PolygonStyle());
                            polygonsList.Add(polygon);
                        }

                        // Pen
                        line = sr.ReadLine().Split(' ');
                        var width = float.Parse(line[1].Substring(1, line[0].Length - 2));
                        var penColor = IntToColor(Convert.ToInt32(line[3].Substring(0, line[3].Length - 1)));

                        // Brush
                        line = sr.ReadLine().Split(' ');
                        var brushColor = IntToColor(Convert.ToInt32(line[2].Substring(0, line[2].Length - 1)));

                        // Добавление полигонов в основной список и задание стилей 
                        foreach (var polygon in polygonsList)
                        {
                            polygon.LineStyle.Width = width;
                            polygon.LineStyle.Color = penColor;
                            polygon.PolygonStyle.Color = brushColor;
                            vectorLayer.AddMapObject(polygon);
                        }
                    }
                }
            }

            return vectorLayer;
        }

        private static Color IntToColor(int dec)
        {
            var red = (byte) ((dec >> 16) & 0xff);
            var green = (byte) ((dec >> 8) & 0xff);
            var blue = (byte) (dec & 0xff);
            return Color.FromArgb(red, green, blue);
        }

        public class Column
        {
            public Column(string name, string type)
            {
                Name = name;
                Type = type;
            }

            public string Name { get; set; }

            public string Type { get; set; }
        }
    }
}
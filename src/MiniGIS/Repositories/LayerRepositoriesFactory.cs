﻿namespace MiniGIS.Repositories
{
    using System.IO;

    using MiniGIS.Interfaces;

    public class LayerRepositoriesFactory : ILayerRepositoriesFactory
    {
        public ILayersRepository<BaseLayer> GetLayersRepository(string filePath)
        {
            var extension = Path.GetExtension(filePath);
            switch (extension)
            {
                case ".txt":
                case ".csv":
                case ".tsv":
                    return new TxtLayerRepository(filePath);

                case ".mif":
                    return new MifLayerRepository(filePath);

                case ".json":
                    return new JsonLayerRepository(filePath);

                default:
                    return null;
            }
        }
    }
}
﻿namespace MiniGIS.Repositories
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Text;

    using MiniGIS.Interfaces;
    using MiniGIS.Vector;

    public class TxtLayerRepository : IVectorLayersRepository
    {
        private readonly string _filePath;

        public TxtLayerRepository(string filePath)
        {
            _filePath = filePath;
        }

        public VectorLayer GetLayer()
        {
            var layer = new VectorLayer(Path.GetFileNameWithoutExtension(_filePath));
            using (var sr = new StreamReader(_filePath, Encoding.UTF8))
            {
                var points = new List<GeoPoint>();

                while (!sr.EndOfStream)
                {
                    var pointCoordinates = sr.ReadLine()?.Split('\t', ';', ':', ' ', '|', '~');
                    if (pointCoordinates == null || pointCoordinates.Length != 3)
                        continue;

                    var x = ParseDoubleValue(pointCoordinates[0]);
                    var y = ParseDoubleValue(pointCoordinates[1]);
                    var z = ParseDoubleValue(pointCoordinates[2]);

                    var point = new GeoPoint(x, y, z);
                    points.Add(point);
                }

                points.ForEach(p => layer.AddMapObject(new Point(p)));
            }

            return layer;
        }

        private static double ParseDoubleValue(string value)
        {
            var commaDoubleValue = value.Replace('.', ',');
            return double.Parse(commaDoubleValue, CultureInfo.CreateSpecificCulture("ru-RU"));
        }
    }
}
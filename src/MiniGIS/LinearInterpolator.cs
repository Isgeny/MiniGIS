﻿namespace MiniGIS
{
    using MiniGIS.Interfaces;

    public class LinearInterpolator : IPointsInterpolator
    {
        public LinearInterpolator()
        {
            Y1 = 8;
            Y2 = 32;
        }

        public double X1 { get; set; }

        public double X2 { get; set; }

        public double Y1 { get; set; }

        public double Y2 { get; set; }

        public double Interpolate(double value)
        {
            var dx = X2 - X1;
            var denominator = dx != 0.0 ? dx : 1.0;
            var multiplier = (Y2 - Y1) / denominator;
            return (value - X1) * multiplier + Y1;
        }
    }
}